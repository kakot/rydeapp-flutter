import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/Tabs/ListOfSub.dart';
import 'package:rydeapp/classes/http/Subscribers.dart';
import 'package:rydeapp/classes/http/ThirdPageHttp.dart';
import 'package:share/share.dart';

class OrgSection {
  ThirdPageHttp thirdPageHttp = new ThirdPageHttp();
  bool isorgloading = false;
  Widget orgpage(_scaff, bus, sad, setme) {
    return new FutureBuilder(
      future: thirdPageHttp.getorganised(),
      builder: ((BuildContext context, AsyncSnapshot snapshot) {
        if (!(snapshot.hasData)) {
          return new Center(
            child: new CircularProgressIndicator(),
          );
        } else if (snapshot.data['data'].length == 0) {
          return new RefreshIndicator(
              onRefresh: () async {
                return new Future.delayed(const Duration(milliseconds: 1000));
              },
              child: new Container(
                margin: EdgeInsets.all(80.0),
                child: new Column(
                  children: <Widget>[
                    new Image(
                      image: sad,
                      //height: 0.0,
                    ),
                    new Container(
                      child: new Text('No Trip'),
                    )
                  ],
                ),
              ));
        } else {
          var body = snapshot.data['data'];
          var count = body.length;
          return new GestureDetector(
            onTap: () {},
            child: new Container(
              margin: EdgeInsets.all(10.0),
              child: new RefreshIndicator(
                  onRefresh: () async {
                    // GetOrganised getOrganised = new GetOrganised();
                    // await getOrganised.getorganised();
                    return new Future.delayed(
                        const Duration(milliseconds: 1000));
                  },
                  child: new Stack(
                    children: <Widget>[
                      new ListView.builder(
                          itemCount: count,
                          itemBuilder: (BuildContext context, int index) {
                            return new Container(
                                // margin: EdgeInsets.all(10.0),
                                child: new Card(
                              elevation: 2.0,
                              child: new ListTile(
                                onLongPress: () async {
                                  print(body[index]);
                                  print('top');
                                  setme(isorgloading = true);
                                  print('this is isloading $isorgloading');
                                  GetSubcribers gs = new GetSubcribers();
                                  var data = await gs.getSub(body[index]['id']);
                                  setme(isorgloading = false);
                                  print('below');
                                  int days;

                                  try {
                                    print(data);
                                    DateTime d =
                                        DateTime.fromMillisecondsSinceEpoch(
                                            int.parse(data['expiry']),
                                            isUtc: true);
                                    days = d.difference(DateTime.now()).inDays;
                                  } catch (e) {
                                    print(e);
                                    days = 0;
                                  }
                                  AlertDialog al = new AlertDialog(
                                    contentPadding: EdgeInsets.only(
                                        bottom: 0.0,
                                        left: 24.0,
                                        right: 24.0,
                                        top: 20.0),
                                    content: new Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Container(
                                          decoration: new BoxDecoration(
                                              color: Colors.orange),
                                          child: new Icon(
                                            Icons.account_balance_wallet,
                                            size: 80.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 20.0),
                                          child: new Text(
                                            'Please proceed to payment to view subscribers',
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        new Container(
                                          width: double.infinity,
                                          child: new MaterialButton(
                                            color: Colors.orange,
                                            child: new Text('Pay Now'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              print('here!');
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Payscreen(
                                                          fromTrips: false,
                                                          price:
                                                              '${body[index]['PRICE']}',
                                                          description:
                                                              'Paying for a Organizer',
                                                          clientref:
                                                              '${body[index]['CLIENTREF']}',
                                                          paymentType: 'org',
                                                          item:
                                                              '${body[index]['PICK_UP_POINT']} - ${body[index]['DESTINATION']}',
                                                        )),
                                              );
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                  AlertDialog a = new AlertDialog(
                                    contentPadding:
                                        EdgeInsets.only(bottom: 0.0),
                                    content: new AnimatedContainer(
                                      duration: new Duration(milliseconds: 500),
                                      child: new Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            color: Colors.orange,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                15,
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Pick up point: ${body[index]['PICK_UP_POINT']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Destination: ${body[index]['DESTINATION']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Number of passengers: ${body[index]['NO_OF_PASSENGERS']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Price per seat: Ghs ${body[index]['PRICE_PER_SEAT']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Number of vehicles: ${body[index]['NO_OF_VEHICLES']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Vehicle type: ${body[index]['VEHICLE_TYPE']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: data['income'] == null
                                                ? new Text('Income: Ghs0',
                                                    style: new TextStyle(
                                                        fontSize: 15.0))
                                                : new Text(
                                                    'Income: Ghs${data['income']}',
                                                    style: new TextStyle(
                                                        fontSize: 15.0),
                                                  ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Days: ${days}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Link: ${data['link']}',
                                              textAlign: TextAlign.center,
                                              style: new TextStyle(
                                                fontSize: 15.0,
                                              ),
                                            ),
                                          ),
                                          new Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              new Container(
                                                child: new IconButton(
                                                  icon: new Icon(Icons.share),
                                                  onPressed: () {
                                                    Share.share(
                                                        'Click here to join ${body[index]['NAME']}${data['link']}');
                                                  },
                                                ),
                                              ),
                                              new Container(
                                                child: new IconButton(
                                                  icon: new Icon(
                                                      Icons.content_copy),
                                                  onPressed: () {
                                                    Clipboard.setData(
                                                        new ClipboardData(
                                                            text:
                                                                '${data['link']}'));
                                                    Navigator.of(context).pop();
                                                    _scaff.currentState
                                                        .showSnackBar(
                                                            new SnackBar(
                                                      content: new Text(
                                                          'Copied Link'),
                                                      duration: new Duration(
                                                          milliseconds: 1000),
                                                    ));
                                                  },
                                                ),
                                              )
                                            ],
                                          ),
                                          new Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                16,
                                            width: double.infinity,
                                            // margin: EdgeInsets.all(15.0),
                                            child: new RaisedButton(
                                              color: Colors.orange,
                                              onPressed: () async {
                                                Navigator.of(context).pop();
                                                Navigator.of(context).push(
                                                    new MaterialPageRoute(
                                                        builder: ((build) {
                                                  return new ListSub(data);
                                                })));
                                              },
                                              child:
                                                  new Text('View Subscribers'),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                  print(days);
                                  if (days <= 0) {
                                    _scaff.currentState
                                        .showSnackBar(new SnackBar(
                                      content:
                                          new Text('Trip organizer expired'),
                                    ));
                                  } else {
                                    showDialog(
                                      context: context,
                                      child: body[index]['PAID'] ? a : al,
                                    );
                                  }
                                },
                                onTap: () async {
                                  print('this is isloading $isorgloading');
                                  AlertDialog a = new AlertDialog(
                                    contentPadding:
                                        EdgeInsets.only(bottom: 0.0),
                                    content: new AnimatedContainer(
                                      duration: new Duration(milliseconds: 500),
                                      child: new Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            color: Colors.orange,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                15,
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Pick up point: ${body[index]['PICK_UP_POINT']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Destination: ${body[index]['DESTINATION']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Number of passengers: ${body[index]['NO_OF_PASSENGERS']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Price per seat: Ghs ${body[index]['PRICE_PER_SEAT']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Number of vehicles: ${body[index]['NO_OF_VEHICLES']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: new Text(
                                              'Vehicle type: ${body[index]['VEHICLE_TYPE']}',
                                              style:
                                                  new TextStyle(fontSize: 15.0),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                  showDialog(
                                    context: context,
                                    child: a,
                                  );
                                },
                                trailing: body[index]['PAID']
                                    ? new Container(
                                        child: new Text(
                                          'Paid',
                                          style: new TextStyle(
                                              color: Colors.orange),
                                        ),
                                      )
                                    : new Container(
                                        child: new Text(
                                          'Pending',
                                          style: new TextStyle(
                                              color: Colors.orange),
                                        ),
                                      ),
                                subtitle: new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      margin: EdgeInsets.all(2.0),
                                      child: new Text(
                                        'Pick up point: ${body[index]['PICK_UP_POINT']}',
                                        style: new TextStyle(),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(2.0),
                                      child: new Text(
                                        'Destination: ${body[index]['DESTINATION']}',
                                        style: new TextStyle(),
                                      ),
                                    ),
                                  ],
                                ),
                                isThreeLine: true,
                                leading: new Image(
                                  image: bus,
                                  height: 35.0,
                                  color: Colors.orangeAccent,
                                ),
                                title: new Text(
                                  '${body[index]['NAME']}',
                                  style: new TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.orange),
                                ),
                              ),
                            ));
                          }),
                      !isorgloading
                          ? new Container()
                          : new Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.transparent,
                              height: MediaQuery.of(context).size.height,
                              child: new Center(
                                child: new CircularProgressIndicator(),
                              ),
                            )
                    ],
                  )),
            ),
          );
        }
      }),
    );
  }
}
