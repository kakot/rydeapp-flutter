import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';

class PostFeedback {
  var client = new http.Client();

  postFeed(String title, feedback) async {
    Map<String, dynamic> body;
    try {
      await client.post('https://ryde-rest.herokuapp.com/feedback', headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': token,
      }, body: {
        'title': title,
        'feedback': feedback,
      }).then((res) {
        body = json.decode(res.body);
        if (body['status'] == 'success') {
          print('success');
        }
      });
    } catch (e) {}
    print(body);
    return body;
  }
}
