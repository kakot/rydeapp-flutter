import 'package:flutter/material.dart';
import 'dart:async';

class RegisterSplash extends StatefulWidget {
  @override
  _RegisterSplashState createState() => new _RegisterSplashState();
}

class _RegisterSplashState extends State<RegisterSplash> with SingleTickerProviderStateMixin{

  Animation _animation;
  AnimationController _controller;

    var logo = new AssetImage('assets/rrydelogo1.jpg');

@override
initState(){
  _controller = new AnimationController(
    vsync: this,
    duration: new Duration(
      milliseconds: 4000
    ),
  );


  _animation = new ColorTween(
    begin: Colors.black,
    end: Colors.transparent,
  ).animate(_controller);

  _controller.addListener((){
    this.setState((){
    });
  });
  _controller.forward();
  Timer(Duration(milliseconds: 4000),()=>Navigator.of(context).pushReplacementNamed('/home'));
  super.initState();
}


@override
void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body:new Center(
        child:new Column(
          mainAxisAlignment: MainAxisAlignment.center ,
          crossAxisAlignment: CrossAxisAlignment.center ,
          children: <Widget>[
            new Text('Welcome!',style: new TextStyle(
             fontSize: 40.0,
             color: _animation.value,
           ),),
           new Image(
             image: logo,
           ),
          ],
        ),
      ) ,
    );
  }
}
