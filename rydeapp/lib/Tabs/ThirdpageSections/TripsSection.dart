import 'package:flutter/material.dart';
import 'package:rydeapp/Dashboard/DashboardBuilder.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/classes/http/ThirdPageHttp.dart';
import 'package:rydeapp/classes/qrimage.dart';
import 'package:rydeapp/customWidgets.dart';

class TripsSection {
  ThirdPageHttp thirdPageHttp = new ThirdPageHttp();

  Widget tripstab(ImageProvider im, _scaff, bus, sad) {
    print('calling trips');
    return new FutureBuilder(
      future: thirdPageHttp.gettrips(),
      builder: ((BuildContext context, AsyncSnapshot snapshot) {
        if (!(snapshot.hasData)) {
          return new Center(
            child: new CircularProgressIndicator(),
          );
        } else if (snapshot.data['trips'].length == 0) {
          return new RefreshIndicator(
              onRefresh: () async {
                return new Future.delayed(const Duration(milliseconds: 1000));
              },
              child: new Container(
                margin: EdgeInsets.all(80.0),
                child: new Column(
                  children: <Widget>[
                    new Image(
                      image: sad,
                      //height: 0.0,
                    ),
                    new Container(
                      child: new Text('No Trip'),
                    )
                  ],
                ),
              ));
        } else {
          var body = snapshot.data['trips'];
          var count = body.length;
          print(count);
          // return new Container(
          //     margin: EdgeInsets.all(10.0),
          //     child: new RefreshIndicator(
          //       onRefresh: () async {
          //         // GetTrip gt = new GetTrip();
          //         // await gt.gettrips();
          //         return new Future.delayed(const Duration(milliseconds: 1000));
          //       },
          //       child: new ListView.builder(
          //         itemCount: count == null ? 0 : count,
          //         itemBuilder: (BuildContext context, int index) {
          //           return new GestureDetector(
          //             onTap: () {
          //               Navigator.of(context).pushNamed('/tripinfo');
          //             },
          //             child: new Card(
          //               // color: Colors.orange[100],
          //               elevation: 2.0,
          //               child: new Container(
          //                 child: new ListTile(
          //                   trailing: body[index]['user_trip']['PAID']
          //                       ? new Container(
          //                           child: new Text(
          //                             'Paid',
          //                             style:
          //                                 new TextStyle(color: Colors.orange),
          //                           ),
          //                         )
          //                       : new Container(
          //                           child: new Text(
          //                             'Pending',
          //                             style:
          //                                 new TextStyle(color: Colors.orange),
          //                           ),
          //                         ),
          //                   onTap: () {
          //                     AlertDialog a = new AlertDialog(
          //                       contentPadding: EdgeInsets.all(0.0),
          //                       content: new Container(
          //                         child: new Column(
          //                           mainAxisSize: MainAxisSize.min,
          //                           crossAxisAlignment:
          //                               CrossAxisAlignment.start,
          //                           children: <Widget>[
          //                             new Container(
          //                               color: Colors.orange,
          //                               height:
          //                                   MediaQuery.of(context).size.height /
          //                                       15,
          //                             ),
          //                             new Container(
          //                               margin: EdgeInsets.all(10.0),
          //                               child: new Text(
          //                                 'Route: ${body[index]['ROUTE']}',
          //                                 style: new TextStyle(fontSize: 15.0),
          //                               ),
          //                             ),
          // new Container(
          //   margin: EdgeInsets.all(10.0),
          //   child: new Text(
          //     'Morning Departure Time: ${body[index]['MORNING']}',
          //     style: new TextStyle(fontSize: 15.0),
          //   ),
          // ),
          // new Container(
          //   margin: EdgeInsets.all(10.0),
          //   child: new Text(
          //     'Evening Departure Time: ${body[index]['EVENING']}',
          //     style: new TextStyle(fontSize: 15.0),
          //   ),
          // ),
          // new Container(
          //   margin: EdgeInsets.all(10.0),
          //   child: new Text(
          //     'Seats checked out: ${body[index]['user_trip']['SEATS']}',
          //     style: new TextStyle(fontSize: 15.0),
          //   ),
          // ),
          // new Container(
          //   margin: EdgeInsets.all(10.0),
          //   child: new Text(
          //     'Car description: ${body[index]['DESCRIPTION']}',
          //     style: new TextStyle(fontSize: 15.0),
          //   ),
          // ),
          // new Container(
          //   margin: EdgeInsets.all(10.0),
          //   child: new Text(
          //     'Car number: ${body[index]['CAR_NO']}',
          //     style: new TextStyle(fontSize: 15.0),
          //   ),
          //                             ),
          //                           ],
          //                         ),
          //                       ),
          //                     );

          //                     showDialog(
          //                       context: context,
          //                       child: a,
          //                     );
          //                   },
          //                   onLongPress: () {
                              // DateTime d = DateTime.fromMillisecondsSinceEpoch(
                              //     int.parse(body[index]['user_trip']['EXP']),
                              //     isUtc: true);
                              // int days = d.difference(DateTime.now()).inDays;
          //                     var qr = new AlertDialog(
          //                         contentPadding: EdgeInsets.all(0.0),
          //                         title: new Text(
          //                           'Ticket:  ${body[index]['ROUTE']}',
          //                           style: new TextStyle(fontSize: 15.0),
          //                         ),
          //                         content: new Container(
          //                             // height: 350.0,
          //                             //width: 200.0,
          //                             child: new Container(
          //                                 child: new Column(
          //                           mainAxisSize: MainAxisSize.min,
          //                           children: <Widget>[
          //                             new Container(
          //                               child: new QrImage(
          //                                 version: 5,
          //                                 data:
          //                                     '${body[index]['ROUTE']}>${body[index]['id']}>${body[index]['user_trip']['EXP']}',
          //                                 size: 300.0,
          //                                 // padding: EdgeInsets.only(left: 8.0),
          //                               ),
          //                             ),
          //                             new Container(
          //                               child: new Text(
          //                                 'Expires in $days days',
          //                                 style: new TextStyle(fontSize: 15.0),
          //                               ),
          //                             ),
          //                           ],
          //                         ))));
          //                     AlertDialog al = new AlertDialog(
          //                       contentPadding: EdgeInsets.only(
          //                           bottom: 0.0,
          //                           left: 24.0,
          //                           right: 24.0,
          //                           top: 20.0),
          //                       content: new Column(
          //                         mainAxisSize: MainAxisSize.min,
          //                         children: <Widget>[
          //                           new Container(
          //                             decoration: new BoxDecoration(
          //                                 color: Colors.orange),
          //                             child: new Icon(
          //                               Icons.account_balance_wallet,
          //                               size: 80.0,
          //                               color: Colors.white,
          //                             ),
          //                           ),
          //                           new Container(pay
          //                             margin: EdgeInsets.only(top: 20.0),
          //                             child: new Text(
          //                               'Please proceed to payment and activate ticket',
          //                               textAlign: TextAlign.center,
          //                             ),
          //                           ),
          //                           new Container(
          //                             width: double.infinity,
          //                             child: new MaterialButton(
          //                               color: Colors.orange,
          //                               child: new Text('Pay Now'),
          //                               onPressed: () {
          //                                 print(
          //                                     '${body[index]['user_trip']['SEATS']}');
          //                                 Navigator.push(
          //                                   context,
          //                                   MaterialPageRoute(
          //                                       builder: (context) => Payscreen(
          //                                             fromTrips: false,
          //                                             seatN:
          //                                                 '${body[index]['user_trip']['SEATS']}',
          //                                             description:
          //                                                 'paying for a Trip',
          //                                             tripid:
          //                                                 '${body[index]['id']}',
          //                                             paymentType: 'trip',
          //                                             item:
          //                                                 '${body[index]['ROUTE']}',
          //                                             duration: 'monthly',
          //                                           )),
          //                                 );
          //                               },
          //                             ),
          //                           )
          //                         ],
          //                       ),
          //                     );
          //                     if (days <= 0) {
          //                       showDialog(
          //                         context: context,
          //                         child: body[index]['user_trip']['PAID']
          //                             ? qr
          //                             : al,
          //                       );
          //                     } else {
          //                       _scaff.currentState.showSnackBar(new SnackBar(
          //                         content: new Text('Trip Expired'),
          //                       ));
          //                     }
          //                   },
          //                   leading: new Image(
          //                     image: bus,
          //                     height: 35.0,
          //                     color: Colors.orangeAccent,
          //                   ),
          //                   title: new Text(
          //                     '${body[index]['ROUTE']}',
          //                     style: new TextStyle(
          //                         fontSize: 18.0,
          //                         fontWeight: FontWeight.bold,
          //                         color: Colors.orange),
          //                   ),
          //                   isThreeLine: true,
          //                   subtitle: new Column(
          //                     mainAxisAlignment: MainAxisAlignment.start,
          //                     crossAxisAlignment: CrossAxisAlignment.start,
          //                     children: <Widget>[
          //                       new Container(
          //                         margin: EdgeInsets.all(2.0),
          //                         child: new Text(
          //                           'Morning Time: ${body[index]['MORNING']}',
          //                           style: new TextStyle(),
          //                         ),
          //                       ),
          //                       new Container(
          //                         margin: EdgeInsets.all(2.0),
          //                         child: new Text(
          //                           'Evening Time: ${body[index]['EVENING']}',
          //                           style: new TextStyle(),
          //                         ),
          //                       ),
          //                     ],
          //                   ),
          //                 ),
          //               ),
          //             ),
          //           );
          //         },
          //       ),
          //     ));

          return new Container(
            child: new ListView.builder(
              itemCount: count,
              itemBuilder: ((context, index) {
                return new DashboardBuilder(body[index], bus, index, count,
                    function1:(h) {
                  print(h);
                  payNow(body, context, index);
                },function2:(){
                  showD(body, index, context);
                });
              }),
            ),
          );
        }
      }),
    );
  }

  payNow(body, context, index) {
    print('${body[index]['user_trip']['SEATS']}');
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Payscreen(
                fromTrips: false,
                seatN: '${body[index]['user_trip']['SEATS']}',
                description: 'paying for a Trip',
                tripid: '${body[index]['id']}',
                paymentType: 'trip',
                item: '${body[index]['ROUTE']}',
                duration: 'monthly',
              )),
    );
  }

  showD(body, index,context) {
                                  DateTime d = DateTime.fromMillisecondsSinceEpoch(
                                  int.parse(body[index]['user_trip']['EXP']),
                                  isUtc: true);
                              int days = d.difference(DateTime.now()).inDays;
    var qr = new AlertDialog(
        contentPadding: EdgeInsets.all(0.0),
        title: new Text(
          'Ticket:  ${body[index]['ROUTE']}',
          style: new TextStyle(fontSize: 15.0),
        ),
        content: new Container(
            // height: 350.0,
            //width: 200.0,
            child: new Container(
                child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Container(
              child: new QrImage(
                version: 5,
                data:
                    '${body[index]['ROUTE']}>${body[index]['id']}>${body[index]['user_trip']['EXP']}',
                size: 300.0,
                // padding: EdgeInsets.only(left: 8.0),
              ),
            ),
            new Container(
              child: new Text(
                'Expires in $days days',
                style: new TextStyle(fontSize: 15.0),
              ),
            ),
          ],
        ))));
        showDialog(
          child: qr,
          context: context
        );
  }
}
