import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:map_view/map_view.dart';
import 'package:rydeapp/Interfaces/home.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/classes/BingMapsApi.dart';
import 'package:rydeapp/classes/CoachMarks.dart';
import 'package:rydeapp/classes/mappageclass.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/classes/DateandTime.dart';
import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter_places_dialog/flutter_places_dialog.dart';

var client = new http.Client();

DateandTime fit = new DateandTime();
DateandTime fit1 = new DateandTime();

class Rentalform extends StatefulWidget {
  @override
  _RentalformState createState() => new _RentalformState();
}

bool isloading = false;
var backg = new AssetImage('assets/reseat.jpg');
var bus = new AssetImage('assets/bus.png');

String content =
    'Do you have a unique request and need a vehicle to rent or for haulage/delivery services? You are at the right place!';

class _RentalformState extends State<Rentalform> {
  final formKey = GlobalKey<FormState>();
  final dateFormat = DateFormat("MM-d-yyyy");
  var logo = new AssetImage('assets/rrydelogo1.jpg');
  double leftpadding = 25.0;
  double topdownpadding = 10.0;

  TextEditingController startdate = new TextEditingController();
  TextEditingController enddate = new TextEditingController();
  TextEditingController duration = new TextEditingController();
  TextEditingController pickup = new TextEditingController();
  TextEditingController destination = new TextEditingController();
  int _selected = 0;
  int i = 0;
  String from;
  String to;
  String inicar;
  String iniserv;
  int inidur;
  String dur;
  DateTime start;
  DateTime end;

  String pickupName = 'Please select a pick up point';
  String destName = 'Please select a destination';

  void changepointpick() async {
    try {
      PlaceDetails place = await FlutterPlacesDialog.getPlacesDialog();
      print(place.name);
      setState(() {
        pickupName = place.name;
      });
    } catch (e) {
      pickupName = 'Please select a pick up point';
    }
  }

  void changeDestination() async {
    try {
      PlaceDetails place = await FlutterPlacesDialog.getPlacesDialog();
      print(place.name);
      setState(() {
        destName = place.name;
      });
    } catch (e) {
      destName = 'Please select a destination';
    }
  }
  @override
  void initState() {
    FlutterPlacesDialog.setGoogleApiKey(apikey);
    from = places.elementAt(0);
    to = places.elementAt(0);
    inicar = cars.elementAt(0);
    iniserv = cars.elementAt(0);
    inidur = rentduration.elementAt(0);
    WidgetsBinding.instance.addPostFrameCallback((_) => coachTip());
    super.initState();
  }

  @override
  void dispose() {
    print('Disposed!');
    super.dispose();
  }

  List<String> serv = ['Rental', 'Haulage'];
  bool showThumbnail = false;
  Map<String, dynamic> _jsonBody;
  
  void postForm() async {
    dur = calcDatediff(start, end);
    final form = formKey.currentState;
    setState(() {
      isloading = true;
    });
    if (form.validate() &&
        !destName.contains('select a') &&
        !pickupName.contains('select a')) {
      await client.get(
          'https://ryde-rest.herokuapp.com/vehicles/${serv[_selected]}-$inicar',
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "x-access-token": token
          }).then((res) {
        _jsonBody = json.decode(res.body);
        print(
            'https://ryde-rest.herokuapp.com/${_jsonBody['businessVehicles'][0]['vehicles'][0]['IMAGE_URLS']}');
      });
      setState(() {
        showThumbnail = true;
      });
    } else {
      String data = 'Please check if locations have been picked';
      _scaff.currentState.showSnackBar(new SnackBar(
        content: new Text(data),
      ));

      setState(() {
        isloading = false;
      });
    }
    setState(() {
      isloading = false;
    });
  }

  Future<Null> postActual() async {
    await client.post('https://ryde-rest.herokuapp.com/rent', headers: {
      "Accept": "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": token
    }, body: {
      "vehicle": inicar,
      'vehicleId':
          _jsonBody['businessVehicles'][0]['vehicles'][0]['id'].toString(),
      'businessId': (_jsonBody['businessVehicles'][0]['vehicles'][0]
              ['BUSINESS_ID'])
          .toString(),
      'pickup_time': '10:00Am',
      "service": serv[_selected],
      'duration': dur,
      'pick_up_point': pickupName,
      'destination': destName,
      'start': startdate.text.trim(),
      'end': enddate.text.trim(),
    }).then(
      (response) {
        Map<String, dynamic> body = json.decode(response.body);
        if (body['status'] == 'success') {
          print(body);
          setState(() {
            showThumbnail = true;
          });
          var err = new AlertDialog(
              contentPadding: EdgeInsets.all(0.0),
              title: new Center(
                  child: new Container(
                      color: Colors.orangeAccent,
                      child: new Center(
                          child: new Icon(
                        Icons.mail_outline,
                        color: Colors.white,
                        size: 120.0,
                      )))),
              content: new Container(
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                        child: new Text(
                      '${body['message']} and can be viewed on the Dashboard',
                      textAlign: TextAlign.center,
                      softWrap: true,
                    )),
                    new Container(
                      child: new FlatButton(
                        child: new Text(
                          'Ok',
                          style: new TextStyle(
                              color: Colors.orangeAccent, fontSize: 20.0),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                          Navigator.of(context).pushReplacement(
                              new MaterialPageRoute(
                                  builder: (BuildContext context) => Home(0)));
                        },
                      ),
                    ),
                  ],
                ),
              ));
          showDialog(context: context, child: err, barrierDismissible: false);
        } else {
          print(body);
        }
      },
    );
  }

  void _onchangeserv(int se) {
    setState(() {
      _selected = se;
    });
  }

  void _onchangecar(String value) {
    setState(() {
      inicar = value;
    });
  }

  GlobalKey<ScaffoldState> _scaff = GlobalKey<ScaffoldState>();
  GlobalKey _firstIconButton = new GlobalObjectKey('pickip');
  GlobalKey _secondIconButton = new GlobalObjectKey('destination');
  GlobalKey _vehicleCoachKey = new GlobalObjectKey('vehicle');
  GlobalKey _durationCoachKey = new GlobalObjectKey('duration');
  GlobalKey _rentalCoachKey = new GlobalObjectKey('rental');
  GlobalKey _submitButtonCoachKey = new GlobalObjectKey('submit');

  void coachTip() {
    final String _pickCoach =
        'Please click here to select \npickup location from map';
    final String _destCoah =
        'Please click here to select \ndestination location from map';
    final String _vehicleCoach = 'Select type of vehicle';
    final String _dateCoach = 'Select the duration of \nthe rental';
    final String _serviceCoach = 'then select the desired rental type';
    final String _submitCoach =
        'finally click here to create rental\n after creating details can be reviewed on \nthe dashboard';

    new Timer(new Duration(milliseconds: 300), () {
      new CoachMarks(_firstIconButton)
        ..showMark(_pickCoach, BoxShape.circle, function: () {
          new CoachMarks(_secondIconButton)
            ..showMark(_destCoah, BoxShape.circle, function: () {
              new CoachMarks(_vehicleCoachKey)
                ..showMark(_vehicleCoach, BoxShape.rectangle, function: () {
                  new CoachMarks(_durationCoachKey)
                    ..showMark(_dateCoach, BoxShape.rectangle, function: () {
                      new CoachMarks(_rentalCoachKey)
                        ..showMark(_serviceCoach, BoxShape.rectangle,
                            function: () {
                          new CoachMarks(_submitButtonCoachKey)
                            ..showMark(_submitCoach, BoxShape.rectangle);
                        });
                    });
                });
            });
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    return new Scaffold(
      key: _scaff,
      backgroundColor: Colors.orange[100],
      appBar: appbar('Rental', context, '/aboutpage'),
      body: Container(
          child: new AnimatedCrossFade(
        crossFadeState: showThumbnail
            ? CrossFadeState.showSecond
            : CrossFadeState.showFirst,
        duration: new Duration(milliseconds: 500),
        firstCurve: Curves.decelerate,
        secondCurve: Curves.decelerate,
        firstChild: new Container(
          child: new SingleChildScrollView(
              child: new Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.09,
                bottom: 0.0,
                right: 10.0,
                left: 10.0),
            child: new Card(
              elevation: 1.0,
              child: new Form(
                key: formKey,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Center(
                      child: new Image(
                        image: logo,
                        height: 0.1 * MediaQuery.of(context).size.height,
                      ),
                    ),
                    new Container(
                        margin: EdgeInsets.only(
                          left: leftpadding,
                          right: leftpadding * 3,
                        ),
                        child: new Text(
                          'Pick up point: ',
                          style: new TextStyle(color: Colors.orange),
                        )),
                    new Container(
                        margin: EdgeInsets.only(
                            left: leftpadding,
                            right: leftpadding * 3,
                            bottom: leftpadding / 2),
                        child: new Row(
                          children: <Widget>[
                            updatePick(),
                            new Container(
                              child: new IconButton(
                                key: _firstIconButton,
                                onPressed: () async {
                                  
                                    changepointpick();
                                
                                },
                                icon: new Icon(
                                  Icons.location_on,
                                  color: Colors.orange,
                                ),
                              ),
                            )
                          ],
                        )),
                    new Container(
                        margin: EdgeInsets.only(
                          left: leftpadding,
                          right: leftpadding * 3,
                        ),
                        child: new Text(
                          'Destination: ',
                          style: new TextStyle(color: Colors.orange),
                        )),
                    new Container(
                        margin: EdgeInsets.only(
                            left: leftpadding,
                            right: leftpadding * 3,
                            bottom: leftpadding / 2),
                        child: new Row(
                          children: <Widget>[
                            updateDest(),
                            new Container(
                              child: new IconButton(
                                key: _secondIconButton,
                                onPressed: () async {

                                    changeDestination();
                                
                                },
                                icon: new Icon(
                                  Icons.location_on,
                                  color: Colors.orange,
                                ),
                              ),
                            )
                          ],
                        )),
                    new Row(key: _vehicleCoachKey, children: <Widget>[
                      new Container(
                        //width: screenwidth * 0.23,
                        margin: EdgeInsets.only(
                            left: leftpadding, top: topdownpadding),
                        child: new Text(
                          'Vehicle :',
                          style: new TextStyle(
                              fontSize: 15.0, color: Colors.orange),
                        ),
                      ),
                      new Center(
                        child: new Container(
                            margin: EdgeInsets.only(
                                left: leftpadding, top: topdownpadding),
                            // width: 400.0,
                            //margin: EdgeInsets.only(),
                            child: new DropdownButton(
                              isDense: true,
                              onChanged: (dynamic value) {
                                _onchangecar(value);
                              },
                              value: inicar,
                              items: cars.map((String value) {
                                return new DropdownMenuItem(
                                  value: value,
                                  child: new Container(
                                    //width: screenwidth * 0.5,
                                    child: new Text(value),
                                  ),
                                );
                              }).toList(),
                            )),
                      ),
                    ]),
                    new Container(
                      key: _durationCoachKey,
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(
                              left: leftpadding,
                              right: leftpadding * 3,
                            ),
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: DateTimePickerFormField(
                              dateOnly: true,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2017),
                              lastDate: DateTime(2030),
                              initialTime: TimeOfDay.now(),
                              controller: startdate,
                              enabled: true,
                              validator: (val) {
                                if (val == null) {
                                  return 'Cannot leave start date blank';
                                } else if (val.compareTo(end) > 0) {
                                  return 'Start date cannot be greater\nthan end Date';
                                } else {
                                  return null;
                                }
                              },
                              decoration: new InputDecoration(
                                  contentPadding: EdgeInsets.all(1.5),
                                  labelText: 'Start Date',
                                  labelStyle:
                                      new TextStyle(color: Colors.orange)),
                              format: dateFormat,
                              onChanged: (date) {
                                setState(() {
                                  start = date;
                                });
                              },
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(
                              left: leftpadding,
                              right: leftpadding * 3,
                            ),
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: DateTimePickerFormField(
                              dateOnly: true,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2005),
                              lastDate: DateTime(2030),
                              initialTime: TimeOfDay.now(),
                              controller: enddate,
                              enabled: true,
                              validator: (val) => val == null
                                  ? 'Cannot leave End Date blank'
                                  : null,
                              decoration: new InputDecoration(
                                  contentPadding: EdgeInsets.all(1.5),
                                  labelText: 'End Date',
                                  labelStyle:
                                      new TextStyle(color: Colors.orange)),
                              format: dateFormat,
                              onChanged: (date) {
                                setState(() {
                                  end = date;
                                });
                                calcDatediff(start, end);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      key: _rentalCoachKey,
                      margin: EdgeInsets.only(top: topdownpadding),
                      child: new Row(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Container(
                                  width: screenwidth * 0.15,
                                  margin: EdgeInsets.only(
                                      left: leftpadding, top: topdownpadding),
                                  child: new Text('Rental')),
                              new Container(
                                margin: EdgeInsets.only(left: leftpadding),
                                child: new Radio(
                                  groupValue: _selected,
                                  value: 0,
                                  onChanged: (int value) {
                                    _onchangeserv(value);
                                  },
                                ),
                              ),
                            ],
                          ),
                          new Row(
                            children: <Widget>[
                              new Container(
                                  width: screenwidth * 0.15,
                                  margin: EdgeInsets.only(
                                      left: leftpadding, top: topdownpadding),
                                  child: new Text('Haulage')),
                              new Container(
                                margin: EdgeInsets.only(left: leftpadding),
                                child: new Radio(
                                  groupValue: _selected,
                                  value: 1,
                                  onChanged: (int value) {
                                    _onchangeserv(value);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    !isloading
                        ? new Container(
                            width: 700.0,
                            height: 50.0,
                            margin: EdgeInsets.only(
                              top: 20.0,
                            ),
                            child: new RaisedButton(
                              key: _submitButtonCoachKey,
                              elevation: 5.0,
                              color: Colors.orangeAccent,
                              onPressed: () {
                                postForm();
                              },
                              child: new Text(
                                "Create",
                                style: new TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ))
                        : new Center(
                            child: new CircularProgressIndicator(),
                          ),
                  ],
                ),
              ),
            ),
          )),
        ),
        secondChild: new Container(
            child: new Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.09,
                    bottom: 0.0,
                    right: 10.0,
                    left: 10.0),
                child: new Card(
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Padding(
                        padding: EdgeInsets.all(20.0),
                      ),
                      // new Center(
                      //     child: new Container(
                      //         width: MediaQuery.of(context).size.width / 4,
                      //         child: new Image.network(
                      //             'https://ryde-rest.herokuapp.com/${_jsonBody['businessVehicles'][0]['vehicles'][0]['IMAGE_URLS']}')
                      //             )),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                          "Business information",
                          style: new TextStyle(color: Colors.orange),
                        ),
                      ),
                      new Divider(),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Business name: ${_jsonBody['businessVehicles'][0]['BUSINESS_NAME']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Email: ${_jsonBody['businessVehicles'][0]['EMAIL']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Phone number: ${_jsonBody['businessVehicles'][0]['PHONE_NUMBER']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                          "Vehicle information",
                          style: new TextStyle(color: Colors.orange),
                        ),
                      ),
                      new Divider(),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Car color: ${_jsonBody['businessVehicles'][0]['vehicles'][0]['COLOR']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Car number: ${_jsonBody['businessVehicles'][0]['vehicles'][0]['CAR_NUMBER']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Cost: ${_jsonBody['businessVehicles'][0]['vehicles'][0]['COST']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: _jsonBody == null
                            ? new Text('')
                            : new Text(
                                'Seat number: ${_jsonBody['businessVehicles'][0]['vehicles'][0]['SEATS_NUMBER']}'),
                      ),
                      new Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 22),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            !isloading
                                ? new RaisedButton(
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width /
                                                8,
                                        right:
                                            MediaQuery.of(context).size.width /
                                                8,
                                        top: MediaQuery.of(context).size.width /
                                            24,
                                        bottom:
                                            MediaQuery.of(context).size.width /
                                                24),
                                    onPressed: () async {
                                      setState(() {
                                        isloading = true;
                                      });
                                      await postActual();
                                      setState(() {
                                        isloading = false;
                                      });
                                    },
                                    child: new Text('Confirm'),
                                    color: Colors.orange,
                                  )
                                : new Center(
                                    child: new CircularProgressIndicator(),
                                  ),
                            new RaisedButton(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width / 8,
                                  right: MediaQuery.of(context).size.width / 8,
                                  top: MediaQuery.of(context).size.width / 24,
                                  bottom:
                                      MediaQuery.of(context).size.width / 24),
                              onPressed: () {
                                setState(() {
                                  showThumbnail = false;
                                  isloading = false;
                                });
                              },
                              child: new Text('Cancel'),
                              color: Colors.orange,
                            ),
                            new Padding(
                              padding: EdgeInsets.only(top: 80.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  elevation: 1.0,
                ))),
      )),
    );
  }

  Widget updatePick() {
    // changepointpick();
    return new Container(
        width: MediaQuery.of(context).size.width / 3,
        child: new Text(pickupName));
  }

  Widget updateDest() {
    // changepointpick();
    return new Container(
        width: MediaQuery.of(context).size.width / 3,
        child: new Text(destName));
  }

  String calcDatediff(DateTime st, DateTime end) {
    try {
      return (end.difference(st).inDays).toString();
    } catch (e) {
      return null;
    }
  }
}
