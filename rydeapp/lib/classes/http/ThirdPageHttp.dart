import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';

class ThirdPageHttp {
  Future<Map> getorganised() async {
    var client = new http.Client();
    Map<String, dynamic> jsonRes;
    await client
        .get('https://ryde-rest.herokuapp.com/Userorganized', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    }).then((res) {
      jsonRes = json.decode(res.body);
    });
    if (jsonRes['status'] == 'success') {
      return jsonRes;
    } else {
      return {};
    }
  }

 Future<Map> getrents() async {
    var client = new http.Client();
    Map<String, dynamic> jsonRes;

    await client
        .get('https://ryde-rest.herokuapp.com/userRentals', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token
    }).then((res) {
      jsonRes = json.decode(res.body);
    });
    if (jsonRes['status'] == 'success') {
      return jsonRes;
    } else {
      return {};
    }
  }

  Future gettrips() async {
    Map<String, dynamic> jsonRes;
    var client = new http.Client();
        await client.get('https://ryde-rest.herokuapp.com/userTrips', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    }).then((res) {
      jsonRes = json.decode(res.body);
    });
    if (jsonRes['status'] == 'success') {
      return jsonRes;
    } else {
      return {};
    }
  }
}
