import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rydeapp/classes/CoachMarks.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  String ryderental = 'Basic rental and Haulage services';
  String rydehaulage =
      'This service includes the rental of trucks, delivery vans and pickups for your everyday cargo and delivery activities';
  String event =
      'Create an event or trip and allow anyone to pay towards it. Each created trip comes with a report to keep you up-to-date on payments';
  String content =
      'Everyone needs mobility at one time or the other for one reason or the other. Want to get the best deal? Talk to us. It’s time you took a Ryde.';
  var car = AssetImage('assets/file.jpg');
  var haullogo = AssetImage('assets/truck.jpg');
  var eventlogo = AssetImage('assets/event.jpg');
  var backg = AssetImage('assets/banner1.jpg');
  var bus = new AssetImage('assets/bus.png');

  GlobalKey _firstKey = new GlobalObjectKey('container1');
  GlobalKey _secondKey = new GlobalObjectKey('container2');
  ScrollController _scrollController = new ScrollController();
  void showTip1() {
    final String _rentalCoachMessage =
        "please select to get you \nstarted on rental the service";
    CoachMarks _cm = new CoachMarks(_firstKey)
      ..showMark(_rentalCoachMessage, BoxShape.rectangle,function: (){});
  }

  void scrollMe() {
    _scrollController.animateTo(MediaQuery.of(context).size.height / 2.6,
        curve: Curves.easeIn, duration: new Duration(milliseconds: 1000));
  }

  void timeIntervals() {
    Timer t = new Timer(new Duration(milliseconds: 2000), () {
      showTip1();
    });
    Timer t1 = new Timer(new Duration(milliseconds: 5500), () {
      scrollMe();
    });
    Timer t2 = new Timer(new Duration(milliseconds: 7000), () {
      showTip2();
    });
  }

  @override
  void initState() {
    print('Second Screen');
    WidgetsBinding.instance.addPostFrameCallback((_) => timeIntervals());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void showTip2() async {
   final  String _orgCoachMessage =
        "please select to get you \nstarted on the organiser service";
            CoachMarks _cm = new CoachMarks(_secondKey)
      ..showMark(_orgCoachMessage, BoxShape.rectangle);
  }

  double toppadding = 20.0;
  double bottompadding = 10.0;
  bool checking = true;
  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    return new Scaffold(
      //backgroundColor: Colors.lightBlue[100] ,
      appBar: appbar('Services', context, '/aboutpage'),
      body: new Container(
        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: new Center(
          child: new ListView(
            controller: _scrollController,
            children: <Widget>[
              new Card(
                elevation: 10.0,
                child: new Container(
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: backg,
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: new Container(
                      decoration: new BoxDecoration(
                        color: Colors.orange.withOpacity(0.2),
                      ),
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                            height: 70.0,
                            margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                            child: new Image(
                              image: bus,
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(
                                top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                            child: new Text(
                              'Ryde Logistics',
                              style: new TextStyle(
                                  fontSize: 30.0, color: Colors.white),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(
                                top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                            child: new Text(
                              content,
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  fontSize: 20.0, color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
              new Container(
                child: rentalcard(
                    'Rental',
                    ryderental,
                    car,
                    context,
                    '/rentform',
                    screenwidth,
                    new Container(
                      height: 50.0,
                      width: screenwidth,
                      margin: EdgeInsets.only(
                        top: toppadding,
                      ),
                      child: new RaisedButton(
                        key: _firstKey,
                        color: Colors.orange,
                        onPressed: () {
                          Navigator.of(context).pushNamed('/rentform');
                        },
                        child: new Text(
                          'Start Here',
                          style: new TextStyle(
                              color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    )),
              ),
              new Container(
                child: rentalcard(
                    'Ryde Organiser',
                    event,
                    eventlogo,
                    context,
                    '/createevent',
                    screenwidth,
                    new Container(
                      height: 50.0,
                      width: screenwidth,
                      margin: EdgeInsets.only(
                        top: toppadding,
                      ),
                      child: new RaisedButton(
                        key: _secondKey,
                        color: Colors.orange,
                        onPressed: () {
                          Navigator.of(context).pushNamed('/createevent');
                        },
                        child: new Text(
                          'Start Here',
                          style: new TextStyle(
                              color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onLoad() {}
}
