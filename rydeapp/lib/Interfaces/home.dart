import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'package:rydeapp/Tabs/firstPage.dart';
import 'package:rydeapp/Tabs/secondPage.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/Tabs/fourthPage.dart';
import 'package:rydeapp/main.dart';
import 'package:rydeapp/classes/BingMapsApi.dart';
import 'package:http/http.dart' as http;

BingMapsApi n = new BingMapsApi(bing_api_key);

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
  final int tab;
  Home(this.tab);
}

int currenttab;

var busicon = new AssetImage('assets/bus.png');
var signicon = new AssetImage('assets/sign.png');
var phoneicon = new AssetImage('assets/phone.png');
var qnaicon = new AssetImage('assets/question.png');
var mapicon = new AssetImage('assets/map.png');
var backg = new AssetImage('assets/danfoman.jpg');

double iconheight = 40.0;
double iconwidth = 10.0;

Color bottomcolour = Colors.black;

Color notActive = Colors.black;
Location loc;
Color isActive = Colors.orange;
List<bool> checkcolor = [true, false, false, false, false];
List<int> tab = [0, 1, 2, 3, 4];
OverlayEntry overlayEntry;

class _HomeState extends State<Home> {
  void show(BuildContext context) async {
    OverlayState overlayState = Overlay.of(context);
    RenderBox renderBox = bottomNav.currentContext.findRenderObject();
    print('This is size of render box!!!  ${renderBox.size.width}');
    print('this is render box Constraints!!  ${renderBox.constraints}');
    Offset o = renderBox.localToGlobal(Offset.zero);
    Size size = renderBox.size;
    overlayEntry = new OverlayEntry(builder: ((context) {
      return _build(context, size, o);
    }));

    recursiveShow(overlayState);
  }
    int count = 0;
  recursiveShow(OverlayState overlayState) async {

    await new Future.delayed(const Duration(milliseconds: 300));
    overlayState.insert(overlayEntry);
    await new Future.delayed(const Duration(milliseconds: 300));
    overlayEntry.remove();
    print(count);
    if (count == 5) {
      count = 0;
    } else {
      recursiveShow(overlayState);
      count++;
    }
  }

  Widget _build(BuildContext context, Size size, Offset offset) {
    print('This is offset!! ${offset.dx}');
    return new Positioned(
      top: offset.dy-20.0,
      left: offset.dx-2.0,
      child: new Material(
          color: Colors.transparent,
          child: new Container(
              child: new Icon(
            Icons.add_alert,
            color: Colors.orange,
          ))),
    );
  }

  List<Map<String, dynamic>> loc = [];
  Map<String, double> m = {
    'latit': 0.2,
  };

  var g;
  getr() async {
    Getstation getstation = Getstation();
    g = await getstation.getstation();
    for (int i = 0; i < g.length; i++) {
      loc.add({
        'latitude': g[i]['LAT'],
        'longitude': g[i]['LONG'],
        'areatype': g[i]['DEMANDAREA']
      });
    }
  }

  List<bool> whicIs = [true, false, false, false];
  @override
  void initState() {
    currenttab = widget.tab;
    //  _retrieveDynamicLink();
    WidgetsBinding.instance.addPostFrameCallback(_afterlayout);
    getr();
    super.initState();
  }

  _afterlayout(_) {
    Timer t = new Timer(new Duration(milliseconds: 3000), () {
      show(context);
    });
  }

  GlobalKey bottomNav = new GlobalObjectKey('bottom');
  var holdme;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: holdme = new Stack(
        children: <Widget>[
          new Offstage(
            offstage: currenttab != 0,
            child: new TickerMode(
              enabled: currenttab == 0,
              child: whicIs[0] ? new FirstPage() : new Container(),
            ),
          ),
          new Offstage(
            offstage: currenttab != 1,
            child: new TickerMode(
              enabled: currenttab == 1,
              child: whicIs[1] ? new SecondPage() : new Container(),
            ),
          ),
          new Offstage(
            offstage: currenttab != 2,
            child: new TickerMode(
              enabled: currenttab == 2,
              child: whicIs[2] ? new ThirdPage(0) : new Container(),
            ),
          ),
          new Offstage(
            offstage: currenttab != 3,
            child: new TickerMode(
              enabled: currenttab == 3,
              child: whicIs[3] ? new FourthPage() : new Container(),
            ),
          ),
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          iconSize: 2.0,
          fixedColor: Colors.blue,
          currentIndex: currenttab,
          onTap: (int index) {
            for (var i = 0; i < 4; i++) {
              if (index == i) {
                setState(() {
                  whicIs[index] = true;
                  changecolor(index);
                });
              } else {
                setState(() {
                  whicIs[i] = false;
                });
              }
            }
          },
          items: <BottomNavigationBarItem>[
            new BottomNavigationBarItem(
              icon: new Image(
                color: (checkcolor[0]) ? isActive : notActive,
                image: busicon,
                height: iconheight / 2,
              ),
              title:
                  new Text("Shuttle", style: new TextStyle(color: Colors.black)),
            ),
            new BottomNavigationBarItem(
              icon: new Image(
                image: signicon,
                color: (checkcolor[1]) ? isActive : notActive,
                height: iconheight / 2,
              ),
              title: new Text("Services",
                  style: new TextStyle(color: Colors.black)),
            ),
            new BottomNavigationBarItem(
              icon: new Image(
                key: bottomNav,
                image: phoneicon,
                color: (checkcolor[2]) ? isActive : notActive,
                height: iconheight / 2,
              ),
              title: new Text("Dashboard",
                  style: new TextStyle(color: Colors.black)),
            ),
            new BottomNavigationBarItem(
              icon: new Image(
                image: qnaicon,
                color: (checkcolor[3]) ? isActive : notActive,
                height: iconheight / 2,
              ),
              title: new Text("QnA", style: new TextStyle(color: Colors.black)),
            ),
          ]),
    );
  }

  changecolor(int ind) {
    for (int i = 0; i < checkcolor.length; i++) {
      checkcolor[i] = false;
    }
    currenttab = ind;
    checkcolor[ind] = true;
  }
}

class Getstation {
  Future<List<dynamic>> getstation() async {
    var client = new http.Client();
    String url = 'https://ryde-rest.herokuapp.com/allstations';
    var response = await client.get(url, headers: {'x-access-token': token});
    Map<String, dynamic> body = json.decode(response.body);
    return body['stations'];
  }
}
