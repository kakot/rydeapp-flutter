import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:map_view/map_view.dart';
import 'package:rydeapp/Interfaces/aboutpage.dart';
import 'package:rydeapp/Interfaces/profilepage.dart';
import 'package:rydeapp/Interfaces/ryderental.dart';
import 'package:rydeapp/Interfaces/login.dart';
import 'package:rydeapp/Interfaces/splash.dart';
import 'package:rydeapp/Interfaces/tripspage.dart';
import 'Interfaces/register.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/Interfaces/createevent.dart';
import 'package:rydeapp/Interfaces/registersplash.dart';



double screenwidth = 0.0;
double screenheight = 0.0;
int userid;
String token;
FirebaseUser user;

var nUser;
var bing_api_key = 'AlttOIj3OoyyPhflwbhGOlz3hkuW3QLP2pN5k_-pDTErmXvhSYDxUSsW8NCGd50f';
var apikey = 'AIzaSyA5gm3DbJax7-gDIa7PEsZFrLzFXffUweA';
void main() {
  MapView.setApiKey(apikey);
  SystemChrome.setSystemUIOverlayStyle(
     SystemUiOverlayStyle.light.copyWith(
        statusBarIconBrightness: Brightness.dark,
        statusBarColor: Colors.white
     )
  );
  Timer(new Duration(microseconds: 5000), () {
  });
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/register': (BuildContext context) => new Register(),
        '/splash': (BuildContext context) => new Splash(),
        '/rentform': (BuildContext context) => new Rentalform(),
        '/login': (BuildContext context) => new Login(),
        '/tripinfo': (BuildContext context) => new Trips(),
        '/paypage': (BuildContext context) => new Payscreen(),
        '/createevent': (BuildContext context) => new CreateEvent(),
        '/aboutpage': (BuildContext context) => new AboutPage(),
        '/registersplash': (BuildContext context) => new RegisterSplash(),
        '/profilepage': (BuildContext context) => new ProfilePage(),
      },
      title: 'RYDE',
      theme: new ThemeData(
        primarySwatch: Colors.orange,
        primaryColor: Colors.orange,
        accentColor: Colors.orangeAccent,
        textTheme: new TextTheme(
          body1: TextStyle(fontSize: 15.0),
          body2: TextStyle(fontSize: 18.0,color: Colors.orange)
        )
      ),
      home: new MyHomePage(title: 'Welcome to RYDE!'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Splash(),
    );
  }
}
