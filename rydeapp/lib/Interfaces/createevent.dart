import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_places_dialog/flutter_places_dialog.dart';
import 'package:intl/intl.dart';
import 'package:map_view/location.dart';
import 'package:rydeapp/Interfaces/home.dart';
import 'package:rydeapp/classes/CoachMarks.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/main.dart';
import 'package:http/http.dart' as http;
import 'package:rydeapp/classes/mappageclass.dart';
import 'package:rydeapp/classes/BingMapsApi.dart';

// import 'package:geolocator/geolocator.dart';
var client = new http.Client();

class CreateEvent extends StatefulWidget {
  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEvent> {
  final formKey = GlobalKey<FormState>();
  var logo = new AssetImage('assets/rrydelogo1.jpg');
  double leftpadding = 25.0;
  double topdownpadding = 10.0;

  TextEditingController starttime = new TextEditingController();
  TextEditingController endtime = new TextEditingController();
  TextEditingController passengers = new TextEditingController();
  TextEditingController seats = new TextEditingController();
  TextEditingController zone = new TextEditingController();
  TextEditingController tripName = new TextEditingController();
  TextEditingController vehicles = new TextEditingController();
  String pickupName = 'Please select a pick up point';
  String destName = 'Please select a destination';
  int i = 0;
  String from;
  String to;
  String inicar;
  String iniserv;
  int inidur;

  void changepointpick() async {
    try {
      PlaceDetails place = await FlutterPlacesDialog.getPlacesDialog();
      print(place.name);
      setState(() {
        pickupName = place.name;
      });
    } catch (e) {
      pickupName = 'Please select a pick up point';
    }
  }

  void changeDestination() async {
    try {
      PlaceDetails place = await FlutterPlacesDialog.getPlacesDialog();
      print(place.name);
      setState(() {
        destName = place.name;
      });
    } catch (e) {
      destName = 'Please select a destination';
    }
  }

  bool isloading = false;

  bool _enabled = false;

  @override
  void initState() {
    FlutterPlacesDialog.setGoogleApiKey(apikey);
    print('This is context ${context.toString().contains('CreateEvent')}');
    from = places.elementAt(0);
    to = places.elementAt(0);
    inicar = cars.elementAt(0);
    iniserv = cars.elementAt(0);
    inidur = rentduration.elementAt(0);
    WidgetsBinding.instance.addPostFrameCallback((_) => coachTip());

    super.initState();
  }

  @override
  void dispose() {
    print('Disposed!');
    super.dispose();
  }

  void _onchangecar(String value) {
    setState(() {
      inicar = value;
    });
  }

  var backg = new AssetImage('assets/banner2.jpg');
  var bus = new AssetImage('assets/bus.png');
  void postForm() {
    final form = formKey.currentState;
    setState(() {
      isloading = true;
    });
    if (form.validate() &&
        !destName.contains('select a') &&
        !pickupName.contains('select a')) {
      client.post('https://ryde-rest.herokuapp.com/organize', headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "x-access-token": token
      }, body: {
        'tripName': tripName.text,
        'destination': destName,
        'pick_up_point': pickupName,
        'pickup_time': "10",
        'vehicle_type': inicar.toString(),
        'passengers': passengers.text.trim(),
        'seats': seats.text.trim(),
        'ryde_vehicle': _enabled.toString(),
        'vehicles': vehicles.text.trim(),
      }).then(
        (response) {
          Map<String, dynamic> body = json.decode(response.body);
          setState(() {
            isloading = false;
          });
          if (body['status'] == 'success') {
            var err = new AlertDialog(
                title: new Center(
                    child: new Container(
                        color: Colors.orangeAccent,
                        child: new Center(
                            child: new Icon(
                          Icons.mail_outline,
                          color: Colors.white,
                          size: 120.0,
                        )))),
                content: new Container(
                  //   height: 100.0,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                          child: new Text(
                        'Mail sent to ${body['organizer']['EMAIL']} the Dashboard',
                        textAlign: TextAlign.center,
                        softWrap: true,
                      )),
                      new Container(
                        child: new FlatButton(
                          //color: Colors.orangeAccent,
                          child: new Text(
                            'Ok',
                            style: new TextStyle(
                                color: Colors.orangeAccent, fontSize: 20.0),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();

                            Navigator.of(context).pushReplacement(
                                new MaterialPageRoute(builder: (context) {
                              return new Home(0);
                            }));
                          },
                        ),
                      ),
                    ],
                  ),
                ));
            showDialog(context: context, child: err);
            // GetOrganised go = new GetOrganised();
            // go.getorganised();
          } else {
            print(body);
          }
        },
      );
    } else {
      String data = 'Please check if locations have been picked';
      _scaff.currentState.showSnackBar(new SnackBar(
        content: new Text(data),
      ));
      setState(() {
        isloading = false;
      });
    }
  }

  final timeFormat = DateFormat("h:mm");
  GlobalKey<ScaffoldState> _scaff = GlobalKey<ScaffoldState>();
  GlobalKey _firstIconButton = new GlobalObjectKey('pickip');
  GlobalKey _secondIconButton = new GlobalObjectKey('destination');
  GlobalKey _vehicleCoachKey = new GlobalObjectKey('vehicle');
  GlobalKey _vehiclesCoachKey = new GlobalObjectKey('noOfVehicles');
  GlobalKey _passengerCoachKey = new GlobalObjectKey('passengers');
  GlobalKey _priceCoachKey = new GlobalObjectKey('price');
  GlobalKey _submitButtonCoachKey = new GlobalObjectKey('submit');
  GlobalKey _tripCoachKey = new GlobalObjectKey('trip');

  void coachTip() {
    final String _pickCoach =
        'Please click here to select \npickup location from map';
    final String _destCoah =
        'Please click here to select \ndestination location from map';
    final String _vehicleCoach = 'Select type of vehicle';
    final String _passengersCoach = 'Select the number of passengers';
    final String _priceCoach = 'then select the price per seat';
    final String _vehiclesCoach =
        'then provide the number of\nvehicles you can produce';

    final String _submitCoach =
        'finally click here to create organiser\n after creating details can be reviewed on \nthe dashboard';
    final String _tripCoach = 'Please input trip name';

    Timer t = new Timer(new Duration(milliseconds: 300), () {
      CoachMarks(_tripCoachKey)
        ..showMark(_tripCoach, BoxShape.rectangle, function: () {
          new CoachMarks(_firstIconButton)
            ..showMark(_pickCoach, BoxShape.circle, function: () {
              new CoachMarks(_secondIconButton)
                ..showMark(_destCoah, BoxShape.circle, function: () {
                  new CoachMarks(_vehicleCoachKey)
                    ..showMark(_vehicleCoach, BoxShape.rectangle, function: () {
                      new CoachMarks(_passengerCoachKey)
                        ..showMark(_passengersCoach, BoxShape.rectangle,
                            function: () {
                          new CoachMarks(_priceCoachKey)
                            ..showMark(_priceCoach, BoxShape.rectangle,
                                function: () {
                              new CoachMarks(_vehiclesCoachKey)
                                ..showMark(_vehiclesCoach, BoxShape.rectangle,
                                    function: () {
                                  new CoachMarks(_submitButtonCoachKey)
                                    ..showMark(
                                        _submitCoach, BoxShape.rectangle);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    return new Scaffold(
      key: _scaff,
      backgroundColor: Colors.orange[100],
      appBar: appbar('Organiser', context, '/aboutpage'),
      body: Container(
        child: new Container(
            child: new SingleChildScrollView(
                child: new Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.06,
                        bottom: 0.0,
                        right: 5.0,
                        left: 5.0),
                    child: new GestureDetector(
                      //  onScaleStart: (down){
                      //    print('Tapped $down');
                      //    changepointpick();
                      //  },
                      child: new Card(
                        elevation: 3.0,
                        child: new Form(
                          key: formKey,
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                color: Colors.orange,
                                //  margin: EdgeInsets.all(10.0),
                                padding: EdgeInsets.all(10.0),
                                child: new Text(
                                  'Terms & Conditions: All trips come at a fee and will be accessible to the general public once payment has been completed. Packages are as follows: \n• 2 days (GHS70) \n• 4 days (GHS140) \n• 5 days (GHS170))',
                                  textAlign: TextAlign.left,
                                  style: new TextStyle(color: Colors.white),
                                ),
                              ),
                              new Center(
                                child: new Image(
                                  image: logo,
                                  height:
                                      0.1 * MediaQuery.of(context).size.height,
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(
                                    left: leftpadding,
                                    right: leftpadding * 3,
                                    bottom: leftpadding / 2),
                                child: new TextFormField(
                                  key: _tripCoachKey,
                                  validator: (val) => val.isEmpty
                                      ? 'Please Input Trip Name'
                                      : null,
                                  controller: tripName,
                                  decoration: new InputDecoration(
                                      contentPadding: EdgeInsets.all(1.5),
                                      labelText: 'Trip Name: ',
                                      labelStyle:
                                          new TextStyle(color: Colors.orange)),
                                ),
                              ),
                              new Container(
                                  margin: EdgeInsets.only(
                                    left: leftpadding,
                                    right: leftpadding * 3,
                                  ),
                                  child: new Text(
                                    'Pick up point: ',
                                    style: new TextStyle(color: Colors.orange),
                                  )),
                              new Container(
                                  margin: EdgeInsets.only(
                                      left: leftpadding,
                                      right: leftpadding * 3,
                                      bottom: leftpadding / 2),
                                  child: new Row(
                                    children: <Widget>[
                                      updatePick(),
                                      new Container(
                                        child: new IconButton(
                                          key: _firstIconButton,
                                          onPressed: () async {
                                            changepointpick();
                                          },
                                          icon: new Icon(
                                            Icons.location_on,
                                            color: Colors.orange,
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                              new Container(
                                  margin: EdgeInsets.only(
                                    left: leftpadding,
                                    right: leftpadding * 3,
                                  ),
                                  child: new Text(
                                    'Destination: ',
                                    style: new TextStyle(color: Colors.orange),
                                  )),
                              new Container(
                                  margin: EdgeInsets.only(
                                      left: leftpadding,
                                      right: leftpadding * 3,
                                      bottom: leftpadding / 2),
                                  child: new Row(
                                    children: <Widget>[
                                      updateDest(),
                                      new Container(
                                        child: new IconButton(
                                          key: _secondIconButton,
                                          onPressed: () async {
                                            changeDestination();
                                          },
                                          icon: new Icon(
                                            Icons.location_on,
                                            color: Colors.orange,
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                              new Row(key: _vehicleCoachKey, children: <Widget>[
                                new Container(
                                  margin: EdgeInsets.only(
                                      left: leftpadding, top: topdownpadding),
                                  child: new Text(
                                    'Vehicle Type :',
                                    style: new TextStyle(
                                        fontSize: 15.0, color: Colors.orange),
                                  ),
                                ),
                                new Center(
                                  child: new Container(
                                      margin: EdgeInsets.only(
                                          left: leftpadding / 5,
                                          top: topdownpadding),
                                      // width: 400.0,
                                      //margin: EdgeInsets.only(),
                                      child: new DropdownButton(
                                        onChanged: (dynamic value) {
                                          _onchangecar(value);
                                        },
                                        value: inicar,
                                        items: cars.map((String value) {
                                          return new DropdownMenuItem(
                                            value: value,
                                            child: new Container(
                                              //width: screenwidth * 0.5,
                                              child: new Text(value),
                                            ),
                                          );
                                        }).toList(),
                                      )),
                                ),
                              ]),
                              new Container(
                                margin: EdgeInsets.only(bottom: topdownpadding),
                                child: new Row(
                                  children: <Widget>[
                                    new Container(
                                      width: 130.0,
                                      margin: EdgeInsets.only(
                                        left: leftpadding,
                                      ),
                                      child: new TextFormField(
                                        key: _passengerCoachKey,
                                        validator: (val) {
                                          if (val.isEmpty) {
                                            print('Here! in empty');
                                            return 'Passengers required';
                                          } else if (inicar.contains('loon') &&
                                              int.parse(val) >= 4) {
                                            print('here! in Saloon');
                                            return "Max passengers is 4";
                                          } else if (inicar.contains('an') &&
                                              int.parse(val) >= 15) {
                                            print('here! in Saloon');
                                            return "Max passengers 15";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        controller: passengers,
                                        decoration: new InputDecoration(
                                            contentPadding: EdgeInsets.all(1.5),
                                            hintText: 'eg. 50',
                                            labelText: 'Passengers: ',
                                            labelStyle: new TextStyle(
                                                color: Colors.orange)),
                                      ),
                                    ),
                                    new Container(
                                      width: 130.0,
                                      margin: EdgeInsets.only(
                                        left: leftpadding,
                                        //  right: leftpadding * 3,
                                        //bottom: leftpadding
                                      ),
                                      child: new TextFormField(
                                        key: _priceCoachKey,
                                        validator: (val) => val.isEmpty
                                            ? 'Seat price required'
                                            : null,
                                        keyboardType: TextInputType.number,
                                        controller: seats,
                                        decoration: new InputDecoration(
                                            contentPadding: EdgeInsets.all(1.5),
                                            hintText: 'eg. 70.00',
                                            labelText: 'Price per person: ',
                                            labelStyle: new TextStyle(
                                                color: Colors.orange)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              new Container(
                                child: new Row(
                                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Container(
                                      margin: EdgeInsets.only(),
                                      child: new Row(children: <Widget>[
                                        new Container(
                                          width: 130.0,
                                          margin: EdgeInsets.only(
                                            left: leftpadding,
                                            //right: leftpadding * 3,
                                            bottom: topdownpadding,
                                          ),
                                          child: new TextFormField(
                                            key: _vehiclesCoachKey,
                                            validator: (val) => val.isEmpty
                                                ? 'Vehicles Required'
                                                : null,
                                            keyboardType: TextInputType.number,
                                            controller: vehicles,
                                            decoration: new InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.all(1.5),
                                                hintText: 'eg. 5',
                                                labelText: 'Vehicles: ',
                                                labelStyle: new TextStyle(
                                                    color: Colors.orange)),
                                          ),
                                        ),
                                      ]),
                                    ),
                                  ],
                                ),
                              ),
                              !isloading
                                  ? new Container(
                                      width: 700.0,
                                      height: 50.0,
                                      margin: EdgeInsets.only(
                                        top: 20.0,
                                      ),
                                      child: new RaisedButton(
                                        key: _submitButtonCoachKey,
                                        elevation: 5.0,
                                        color: Colors.orangeAccent,
                                        onPressed: () {
                                          postForm();
                                        },
                                        child: new Text(
                                          "Create",
                                          style: new TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ))
                                  : new Center(
                                      child: new CircularProgressIndicator(),
                                    ),
                            ],
                          ),
                        ),
                      ),
                    )))),
      ),
    );
  }

  Widget updatePick() {
    // changepointpick();
    return new Container(
        width: MediaQuery.of(context).size.width / 3,
        child: new Text(pickupName));
  }

  Widget updateDest() {
    // changepointpick();
    return new Container(
        width: MediaQuery.of(context).size.width / 3,
        child: new Text(destName));
  }
}
