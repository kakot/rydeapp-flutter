import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:permission/permission.dart';
import 'package:rydeapp/Interfaces/IntroScreen.dart';
import 'package:rydeapp/Interfaces/home.dart';
import 'package:rydeapp/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart' as userloc;
import 'package:flutter/services.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

Map<String, double> userlocation = {};

bool checkingUser = true;
FirebaseUser fuser;
bool isloading = false;

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  userloc.Location _loca = new userloc.Location();
  bool _checking = false;
  String _token;
  Animation _animation;
  AnimationController _control;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  getuser() async {
    try {
      await _loca.hasPermission();
      userlocation = await _loca.getLocation();
      print('location called');
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        final res =
            await Permission.requestSinglePermission(PermissionName.Location);
        print(res);
        userlocation = await _loca.getLocation();
        print('Permission denied');
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        print(
            'Permission denied - please ask the user to enable it from the app settings');
      }
      userlocation['longitude'] = -0.2497708;
      userlocation['latitude'] = 5.5912045;
    }
    print('This us user location at splash $userlocation');
  }

  Duration timetoleave = new Duration(milliseconds: 2000);
  Future<SharedPreferences> checklogin = SharedPreferences.getInstance();

  var client = new http.Client();
  Future<String> readlogin() async {
    final SharedPreferences loginpref = await checklogin;
    _token = loginpref.getString('token');
    return _token;
  }

  // readingLogin() async {
  //   setState(() {
  //     _checking = true;
  //   });

  //   var sToken = await readlogin();
  //   if (sToken != null) {
  //     var response = await client.get('https://ryde-rest.herokuapp.com/getUser',
  //         headers: {
  //           "Content-Type": "application/x-www-form-urlencoded",
  //           'x-access-token': sToken
  //         });
  //     Map<String, dynamic> body = json.decode(response.body);
  //     if (body['status'] == 'success') {
  //       token = sToken;
  //       user = body;
  //       Navigator.pushReplacementNamed(context, '/home');
  //     } else {
  //       Navigator.pushReplacementNamed(context, '/login');
  //     }
  //   } else if (sToken == null) {
  //     Navigator.pushReplacementNamed(context, '/login');
  //   }
  // }

  var logo = new AssetImage('assets/rrydelogo1.jpg');
  double sWidth;
  @override
  void initState() {
    getuser();
    isloading = false;
    //setScreenSize();
    super.initState();
  }

  setScreenSize() {
    setState(() {
      screenwidth = MediaQuery.of(context).size.width;
      screenheight = MediaQuery.of(context).size.height;
    });
  }

  // @override
  // void dispose() {
  //   _control.dispose();
  //   super.dispose();
  // }

  // @override
  // Widget build(BuildContext context) {
  //   return Center(
  //     child: new Column(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: <Widget>[
  //         new Container(
  //           child: new Image(
  //             image: logo,
  //             height: 5 + _animation.value * 250,
  //           ),
  //         ),
  //         new Container(
  //           child: new Text(
  //             'Ride with Ryde!',
  //             style: new TextStyle(fontSize: 30.0, color: Colors.orange),
  //           ),
  //         ),
  //         new Padding(
  //           padding:
  //               EdgeInsets.only(top: MediaQuery.of(context).size.height / 9),
  //         ),
  //         _checking
  //             ? new Center(
  //                 child: new CircularProgressIndicator(),
  //               )
  //             : new Container()
  //       ],
  //     ),
  //   );
  // }
  static final FacebookLogin facebookSignIn = new FacebookLogin();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> _handleSignIn() async {
    getuser();
    try {
      GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;

      if (await _googleSignIn.isSignedIn()) {
        fuser = await _auth.signInWithGoogle(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        print("signed in " + fuser.displayName);
        print('${fuser.getIdToken()} THIS IS USER');
        CreateUser _createnewuser = new CreateUser(
            fuser.email,
            fuser.phoneNumber,
            fuser.displayName,
            fuser.photoUrl,
            fuser.providerData[0].uid);
        String userExists = await _createnewuser.checkUser();
        print(userExists);
        if (userExists == 'false') {
          setState(() {
            isloading = false;
            checkingUser = false;
          });
          //await _createnewuser.createUser();
        } else {
          await _createnewuser.getToken(context);
        }
        // Navigator.of(context).pushReplacementNamed('/home');
      } else {
        print('Not Signed in');
      }
    } catch (e) {
      setState(() {
        isloading = false;
      });
    }
    return fuser;
  }

  Future postfacebookLogin() async {
    getuser();
    final FacebookLoginResult result =
        await facebookSignIn.logInWithReadPermissions(['email']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      try {
        fuser = await _auth.signInWithFacebook(
            accessToken: result.accessToken.token);
        CreateUser _createnewuser = new CreateUser(
            fuser.email,
            fuser.phoneNumber,
            fuser.displayName,
            fuser.photoUrl,
            fuser.providerData[0].uid);

        String userExists = await _createnewuser.checkUser();
        print(userExists);
        if (userExists == 'false') {
          setState(() {
            isloading = false;
            checkingUser = false;
          });
          //await _createnewuser.createUser();
        } else {
          await _createnewuser.getToken(context);
        }
      } catch (e) {
        print(e);
        setState(() {
          isloading = false;
        });
      }
      // Navigator.of(context).pushReplacementNamed('/home');
    } else {}
  }

  var backg = new AssetImage('assets/reseat.jpg');
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    setScreenSize();

    return new Stack(
      children: <Widget>[
        new Container(
            height: MediaQuery.of(context).size.height,
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: backg,
                    fit: BoxFit.cover,
                    colorFilter:
                        ColorFilter.mode(Colors.white, BlendMode.softLight)),
                color: Colors.orange),
            child: new Card(
              margin: EdgeInsets.only(top: screenHeight / 2),
              child: new ListView(
                shrinkWrap: true,
                children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.all(20.0),
                  ),
                  new AnimatedCrossFade(
                    firstChild: isloading ? isLoading() : login(),
                    secondChild: isloading ? isLoading() : register(),
                    duration: new Duration(milliseconds: 1000),
                    firstCurve: Curves.easeInOut,
                    secondCurve: Curves.easeIn,
                    crossFadeState: checkingUser
                        ? CrossFadeState.showFirst
                        : CrossFadeState.showSecond,
                  ),
                ],
              ),
            )),
        new Container(
          margin:
              EdgeInsets.only(left: screenWidth / 34, top: screenHeight / 5),
          width: screenWidth / 1.2,
          child: new Image(
            image: logo,
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.colorBurn,
          ),
        )
      ],
    );
  }

  Widget login() {
    return new Center(
      child: new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              child: new Text('Sign in', style: new TextStyle(fontSize: 30.0)),
            ),
            new Container(
              child: new OutlineButton.icon(
                icon: new Icon(
                  Icons.lock,
                  color: Colors.orange,
                ),
                label: new Text('Sign in with Google',
                    style: new TextStyle(color: Colors.orange)),
                onPressed: () async {
                  setState(() {
                    isloading = true;
                  });
                  await _handleSignIn();
                  Timer t = new Timer(
                      new Duration(
                        milliseconds: 3000,
                      ), () {
                    setState(() {
                      isloading = false;
                    });
                  });
                  t.cancel();
                },
                color: Colors.orange,
                textColor: Colors.orange,
                borderSide: new BorderSide(color: Colors.orange),
              ),
            ),
            new Padding(
              padding: EdgeInsets.all(15.0),
            ),
            new Container(
              child: new OutlineButton.icon(
                icon: new Icon(
                  Icons.lock,
                  color: Colors.orange,
                ),
                label: new Text('Sign in with Facebook',
                    style: new TextStyle(color: Colors.orange)),
                onPressed: () async {
                  setState(() {
                    isloading = true;
                  });
                  await postfacebookLogin();
                  Timer t = new Timer(
                      new Duration(
                        milliseconds: 3000,
                      ), () {
                    setState(() {
                      isloading = false;
                    });
                  });
                  t.cancel();
                },
                color: Colors.orange,
                textColor: Colors.orange,
                borderSide: new BorderSide(color: Colors.orange),
              ),
            ),
          ],
        ),
      ),
    );
  }

  final formKey = GlobalKey<FormState>();
  TextEditingController _num = new TextEditingController();
  Widget register() {
    return new Center(
        child: new Form(
            key: formKey,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Container(
                  width: (MediaQuery.of(context).size.width / 2),
                  margin: EdgeInsets.only(top: 30.0, left: 30.0),
                  child: new TextFormField(
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Please enter phone number';
                      } else if (val.length < 10) {
                        return 'Please enter a valid phone number';
                      }
                    },
                    controller: _num,
                    decoration: new InputDecoration(
                        labelText: 'Please enter phone number'),
                    keyboardType: TextInputType.numberWithOptions(),
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 30.0, left: 0.0),
                  child: isloading
                      ? new CircularProgressIndicator()
                      : new IconButton(
                          icon: new Icon(
                            Icons.send,
                            color: Colors.orange,
                          ),
                          // label: isloading
                          //     ? isLoading()
                          //     : new Text('Submit',
                          //         style: new TextStyle(color: Colors.orange)),
                          onPressed: () async {
                            if (formKey.currentState.validate()) {
                              setState(() {
                                isloading = true;
                              });
                              CreateUser _createnewuser = new CreateUser(
                                  fuser.email,
                                  _num.text,
                                  fuser.displayName,
                                  fuser.photoUrl,
                                  fuser.providerData[0].uid);
                              await _createnewuser.createUser(context);
                              Timer t = new Timer(
                                  new Duration(
                                    milliseconds: 3000,
                                  ), () {
                                setState(() {
                                  isloading = false;
                                });
                              });
                              t.cancel();
                            }
                          },
                          color: Colors.orange,
                          // textColor: Colors.orange,
                          // borderSide: new BorderSide(color: Colors.orange),
                        ),
                ),
              ],
            )));
  }

  Widget isLoading() {
    return new Container(
      child: new Center(
        child: new CircularProgressIndicator(),
      ),
    );
  }
}

class CreateUser {
  String email;
  String phone;
  String name;
  String photo;
  String uid;

  final String semail = 'email';
  final String sname = 'name';
  final String sphoto = 'photo';
  final String sphone = 'phone';
  final String suuid = 'uuid';
  CreateUser(String email, phone, name, photo, uid) {
    this.email = email;
    this.phone = phone;
    this.name = name;
    this.photo = photo;
    this.uid = uid;
  }

  final String url = 'https://ryde-rest.herokuapp.com/createuser';

  var client = http.Client();

  Future<dynamic> checkUser() async {
    final String checkuid =
        'https://ryde-rest.herokuapp.com/userexists/${this.uid}';
    var r;
    var resp = await client.get(checkuid).then((res) {
      r = res.body;
    });
    return r;
  }

  Future<Null> getToken(context) async {
    Map<String, dynamic> jsonRes;
    final String getTo = 'https://ryde-rest.herokuapp.com/gettoken/${this.uid}';
    client.get(getTo).then((res) {
      print(res.body);
      jsonRes = json.decode(res.body);
      if (jsonRes['status'] == 'success') {
        token = jsonRes['token'];
        user = fuser;
        nUser = jsonRes;
        // Navigator.of(context).pushReplacement(new MaterialPageRoute(
        //   builder: (BuildContext context)=>new Home(0)
        // ));
        Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) => new IntroScreen()));
      }
    });
  }

  Future<Null> createUser(context) async {
    print('Called');
    print('$email $name $phone $photo');
    Map<String, dynamic> jsonRes;
    var response = await client.post(url, headers: {
      'content-type': 'application/x-www-form-urlencoded'
    }, body: {
      semail: this.email,
      sname: this.name,
      sphone: this.phone,
      sphoto: this.photo,
      suuid: this.uid
    }).then((res) {
      jsonRes = json.decode(res.body);
      print(jsonRes);
      if (jsonRes['status'] == 'success') {
        token = jsonRes['token'];
        user = fuser;
        nUser = jsonRes;
        Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) => new IntroScreen()));
      }
    });
    return null;
  }
}
