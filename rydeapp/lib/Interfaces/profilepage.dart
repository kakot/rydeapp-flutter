import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rydeapp/main.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  File _fromupload;

  var client = new http.Client();

  picker() async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _fromupload = file;
      chosen = true;
    });
  }

  bool chosen = false;
  bool edit = false;
  String imgurl = user.providerData[0].photoUrl;
  @override
  void initState() {
   // getuser();
    print(token);
    super.initState();
    print(imgurl);
  }

  bool isloading = false;

  TextEditingController uname = new TextEditingController();
  TextEditingController uemail = new TextEditingController();
  TextEditingController uphone = new TextEditingController();
  GlobalKey<ScaffoldState> _profilePageState = new GlobalKey();

  var current = new AssetImage('assets/danfoman.jpg');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _profilePageState,
      appBar: appbar('Profile', context, '/aboutpage', showProf: false),
      body: new Container(
          child: new Stack(
        fit: StackFit.loose,
        children: <Widget>[
          new Container(
            child: new Container(
              decoration: new BoxDecoration(
                color: Colors.orange,
              ),
              height: MediaQuery.of(context).size.height / 2,
              child: new Center(
                  child: new GestureDetector(
                      onTap: () {
                        picker();
                      },
                      child: new Stack(
                        children: <Widget>[
                          new Center(
                            child: ClipOval(
                                child: new FadeInImage(
                              fadeInCurve: Curves.easeIn,
                              fadeInDuration: new Duration(milliseconds: 500),
                              fit: BoxFit.cover,
                              placeholder: new AssetImage('assets/user.png'),
                              image: new NetworkImage(imgurl),
                              width: MediaQuery.of(context).size.width / 2,
                              height: MediaQuery.of(context).size.width / 2,
                            )),
                          ),
                          chosen
                              ? new Center(
                                  child: new ClipOval(
                                    child: _fromupload == null
                                        ? new Text('Nothing')
                                        : new Image.file(
                                            _fromupload,
                                            fit: BoxFit.cover,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                          ),
                                  ),
                                )
                              : new Center(),
                          new Container(
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle, color: Colors.white70),
                            margin: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width / 1.8,
                                top: MediaQuery.of(context).size.height / 3),
                            child: new IconButton(
                              icon: new Icon(
                                Icons.edit,
                                size: 40.0,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                picker();
                              },
                            ),
                          )
                        ],
                      ))),
            ),
          ),
          new AnimatedContainer(
             duration: new Duration( milliseconds: 500),
              curve: Curves.fastOutSlowIn,
            margin: EdgeInsets.only(
                top: check
                    ? MediaQuery.of(context).size.height / 9
                    : MediaQuery.of(context).size.height / 2.4),
            decoration: new BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                color: Colors.white),
            child: new ListView(
              children: <Widget>[
                infos(
                    'Name:',
                    nUser['user']['FULL_NAME'] == null
                        ? ''
                        : nUser['user']['FULL_NAME'],
                    Icons.account_circle,
                    uname),
                infos(
                    'Email:',
                    nUser['user']['EMAIL'] == null ? '' : nUser['user']['EMAIL'],
                    Icons.mail,
                    uemail),
                infos(
                    'Phone Number:',
                    nUser['user']['PHONE_NUMBER'] == null
                        ? ''
                        : nUser['user']['PHONE_NUMBER'],
                    Icons.call,
                    uphone),
                new Padding(
                  padding: EdgeInsetsDirectional.only(bottom: 30.0),
                ),
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    new RaisedButton(
                      color: Colors.orange,
                      onPressed: () {
                        setState(() {
                          check=!check;
                          edit = !edit;
                        });
                      },
                      child: edit ? new Text('Reset') : new Text('Edit'),
                    ),
                    !isloading
                        ? new RaisedButton(
                            color: Colors.orange,
                            onPressed: () async {
                              setState(() {
                                isloading = true;
                              });
                              try {
                                // await _postImg();
                                 await updateData();
                              } catch (e) {
                                setState(() {
                                  isloading = false;
                                });
                              }

                              setState(() {
                                isloading = false;
                              });
                            },
                            child: new Text('Update'),
                          )
                        : new Center(
                            child: new CircularProgressIndicator(),
                          )
                  ],
                )
              ],
            ),
          ),
        ],
      )),
    );
  }

  Widget infos(String label, String info, IconData d, TextEditingController t) {
    if (edit) {
      return new Container(
        margin: EdgeInsets.only(
          left: MediaQuery.of(context).size.width / 15,
          right: MediaQuery.of(context).size.width / 15,
        ),
        child: new Column(
          children: <Widget>[
            new Container(
                child: new TextFormField(
              controller: t,
              decoration: new InputDecoration(
                labelText: info,
              ),
            ))
          ],
        ),
      );
    } else {
      return new GestureDetector(
        onLongPress: () {
          Clipboard.setData(new ClipboardData(text: info));
          _profilePageState.currentState.showSnackBar(new SnackBar(
            content: new Text('Copied $info to clipboard'),
          ));
        },
        child: new Container(
          margin: EdgeInsets.all(20.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                child: new Icon(
                  d,
                  color: Colors.orange,
                ),
              ),
              new Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 29),
                child: new Text(
                  label,
                  style: new TextStyle(color: Colors.orange),
                ),
              ),
              new Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 29),
                child: new Text(info),
              )
            ],
          ),
        ),
      );
    }
  }

  bool check = false;
  checkclick() {
    setState(() {
          
        });
    setState(() {
      if (check) {
        check = false;
      } else {
        check = true;
      }
    });
  }

//   _postImg() async {
//     FormData formData;
//     var dio;
//     try {
//       formData = new FormData.from({
//         "file": new UploadFileInfo(
//             new File(_fromupload.path), "${DateTime.now()}user.jpg")
//       });
//       Options options = new Options(
//         baseUrl: "https://ryde-rest.herokuapp.com/",
//         connectTimeout: 8000,
//         receiveTimeout: 3000,
//         headers: {
//           'x-access-token': token,
//         },
//         data: formData,
//         method: 'POST',
//         contentType: ContentType.binary,
//       );
//       dio = new Dio(options);
//     } catch (e) {
//       dio = new Dio();
//     }
//     Response response = await dio.post('/addPhoto', data: formData);
//     print(response.data);
//     getuser();
//   }

  updateData() async {
    var resp = await client
        .post('https://ryde-rest.herokuapp.com/updateprofile', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token
    }, body: {
      'uname': uname.text == '' ? nUser['user']['FULL_NAME'] : uname.text,
      'uemail': uemail.text == '' ? nUser['user']['EMAIL'] : uemail.text,
      'uphone': uphone.text == '' ? nUser['user']['PHONE_NUMBER'] : uphone.text
    }).then((res) {
      Map<String, dynamic> body = json.decode(res.body);
      if (body['status'] == 'success') {
        _profilePageState.currentState.showSnackBar(new SnackBar(
          content: new Text('Successfully updated'),
        ));
        print(body);
        setState(() {
          nUser = body;
        });
      }
      print(body);
    });
  }

//   getuser() async {
//     var res = await client.get('https://ryde-rest.herokuapp.com/getUser',
//         headers: {
//           "Content-Type": "application/x-www-form-urlencoded",
//           'x-access-token': token
//         }).then((res) {
//       Map<String, dynamic> body = json.decode(res.body);
//       if (body['status'] == 'success') {
//         //   print(body);
//         setState(() {
//           nUser = body;
//           imgurl =
//               'https://ryde-rest.herokuapp.com/pic/${body['user']['PHOTO']}';
//         });
//       }
//     });
//   }
 }
