List<String> places = [
    'Ashesi Campus',
    'A&C Mall',
    'Accra Mall',
    'Agbogba Junction',
    'Goil Station - Kwabenya',
    'Adenta Barrier',
    'Madina Market',
    'Atomic Junction Roundabout',
    'Unversity of Ghana',
    'Spanner Junction',
    '37 Military Hospital',
    'Osu Koala',
    'Lapaz',
    'Haatso Junction - Total Filling Station',
    'Achimota Overhead',
    'East Legon, American House',
    'Adjiringano(Rawlings House)',
    'Airport',
    'Spintex Road, Coca Cola Roundabout',
    'Sakumono Junction Mall',
    'National Theatre',
    'North Ridge',
    'Ridge',
    'Adenta Police Station',
    'Osu Mall',
    'W.E.B Dubois Centre - Cantoments',
    "Alliance Francaise d'Accra",
    'Charismatic Evangelical Ministry',
    'Geothe-Institut, Cantoments',
    'Zen Gardens, Labone',
    'Charismatic Evangelical Ministry, North Legon',
    'Afrikiko',
    'British Counsil Auditorium',
    'Silverbird Cinema',
    'Tema Community 2',
  ];
  List<String> cars = [
    'Saloon',
    '4X4(Four Wheel Drive)',
    'Van',
    'Bus'
  ];


  List<String> servtype = ['Ryde Rental','Haulage Rental'];

  List<int> rentduration = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];


List<String> prices = ['GHC 200 for a month','GHC 40 for two weeks','GHC 20 for a week','GHC 3 for three days'];
List<String> orgprice =['GHC 70 for 2 days','GHC 140 for 4 days','GHC 170 for 5 days'];