import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart' as userloc;
import 'package:rydeapp/Tabs/firstPage.dart';
import 'package:rydeapp/main.dart';
import 'package:permission/permission.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

// Map<String, double> userlocation = {};

var client = new http.Client();

TextEditingController email = new TextEditingController();
TextEditingController password = new TextEditingController();

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  userloc.Location _loca = new userloc.Location();
  Future<SharedPreferences> checklogin = SharedPreferences.getInstance();
  static final FacebookLogin facebookSignIn = new FacebookLogin();
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Future<Null> storelogin() async {
  //   final SharedPreferences loginpref = await checklogin;
  //   loginpref.setString('token', token);
  // }

  // storinglogin() async {
  //   await storelogin();
  // }

  // getuser() async {
  //   try {
  //     await _loca.hasPermission();

  //     userlocation = await _loca.getLocation();
  //   } on PlatformException catch (e) {
  //     if (e.code == 'PERMISSION_DENIED') {
  //       final res =
  //           await Permission.requestSinglePermission(PermissionName.Location);
  //       print(res);
  //       userlocation = await _loca.getLocation();
  //       print('Permission denied');
  //     } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
  //       print(
  //           'Permission denied - please ask the user to enable it from the app settings');
  //     }
  //     userlocation['longitude'] = -0.2497708;
  //     userlocation['latitude'] = 5.5912045;
  //   }
  // }

  final formKey = GlobalKey<FormState>();

  bool isloading = false;
  var textcolor = Colors.white;
  var heading = Colors.white;

  double leftandrighdpadding = 10.0;
  double upanddownpadding = 15.0;

  var backg = new AssetImage('assets/reseat.jpg');
  var logo = new AssetImage('assets/rrydelogo1.jpg');

  // void postForm(BuildContext context) {
  //   // if (_token != null) {
  //   // }
  //   final form = formKey.currentState;
  //   setState(() {
  //     isloading = true;
  //   });
  //   if (form.validate()) {
  //     client.post('https://ryde-rest.herokuapp.com/login', headers: {
  //       "Accept": "application/json",
  //       "Content-Type": "application/x-www-form-urlencoded"
  //     }, body: {
  //       "email": email.text.toLowerCase().trim(),
  //       "password": password.text
  //     }).then(
  //       (response) {
  //         Map<String, dynamic> body = json.decode(response.body);
  //         setState(() {
  //           isloading = false;
  //         });
  //         if (body['status'] == 'success') {
  //           user = body;
  //           userid = body['user']['id'];
  //           token = body['token'];
  //           storelogin();
  //           Navigator.of(context).pushReplacementNamed('/home');
  //         } else {
  //           var err = new AlertDialog(
  //             title: new Icon(Icons.face, size: 80.0, color: Colors.orange,),
  //             content: new Text(body['message'], textAlign: TextAlign.center,),
  //           );
  //           showDialog(context: context, child: err);
  //         }
  //       },
  //     );
  //   } else {
  //     setState(() {
  //       isloading = false;
  //     });
  //   }
  // }

  Future<FirebaseUser> _handleSignIn() async {
    GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    FirebaseUser user = await _auth.signInWithGoogle(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    token = googleAuth.accessToken;
    print("signed in " + user.displayName);
    print('${user.getIdToken()} THIS IS USER');
    if (await _googleSignIn.isSignedIn()) {
      Navigator.of(context).pushReplacementNamed('/home');
    } else {
      print('Not Signed in');
    }
    return user;
  }

// Future<void> _handleSignIn() async {
//   try {
//    GoogleSignInAccount acc= await _googleSignIn.signIn();
//    print(acc.displayName);
//   } catch (error) {
//     print('This is error: $error');
//   }
// }
  Future postfacebookLogin() async {
    final FacebookLoginResult result =
        await facebookSignIn.logInWithReadPermissions(['email']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      FirebaseUser user;
      try {
        user = await _auth.signInWithFacebook(
            accessToken: result.accessToken.token);
            token = result.accessToken.token;
      } catch (e) {
        print(e);
      }
      Navigator.of(context).pushReplacementNamed('/home');
    }
  }

  @override
  void initState() {
//    getuser();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(image: backg, fit: BoxFit.cover)),
        child: new Container(
          decoration: new BoxDecoration(color: Colors.orange.withOpacity(0.2)),
          child: new ListView(
              //reverse: true,
              //shrinkWrap: true,
              children: <Widget>[
                new Container(
                  child: new Image(
                    image: logo,
                    height: MediaQuery.of(context).size.height / 4,
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(right: 30.0, left: 30.0),
                  child: new Card(
                    elevation: 50.0,
                    child: new Form(
                      key: formKey,
                      child: new Column(
                        children: <Widget>[
                          !isloading
                              ? new Container(
                                  width: 700.0,
                                  height: 50.0,
                                  margin: EdgeInsets.only(
                                      // top: 20.0,
                                      ),
                                  child: new RaisedButton(
                                    elevation: 5.0,
                                    color: Colors.orangeAccent,
                                    onPressed: () async {
                                      //    Navigator.of(context).pushNamed('/home');
                                      await postfacebookLogin();
                                    },
                                    child: new Text(
                                      "Sign in using Facebook",
                                      style: new TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ))
                              : new CircularProgressIndicator(),
                          !isloading
                              ? new Container(
                                  width: 700.0,
                                  height: 50.0,
                                  margin: EdgeInsets.only(
                                    top: 20.0,
                                  ),
                                  child: new RaisedButton(
                                    elevation: 5.0,
                                    color: Colors.orangeAccent,
                                    onPressed: () async {
                                      await _handleSignIn();
                                    },
                                    child: new Text(
                                      "Sign in using Google",
                                      style: new TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ))
                              : new CircularProgressIndicator(),
                        ],
                      ),
                    ),
                  ),
                )
              ]),
        ),
      ),
    );
  }
}
