import 'package:flutter/material.dart';
import 'package:rydeapp/classes/http/ThirdPageHttp.dart';

class RentalSection{
    ThirdPageHttp thirdPageHttp = new ThirdPageHttp();

    Widget rentalpage(sad,car,truck) {
    AssetImage top;
    Color backg;
    return new FutureBuilder(
        future: thirdPageHttp.getrents(),
      builder: ((BuildContext context, AsyncSnapshot snapshot) {
        if (!(snapshot.hasData)) {
          return new Center(
            child: new CircularProgressIndicator(),
          );
        } else if (snapshot.data['rentals'].length == 0) {
          return new RefreshIndicator(
              onRefresh: () async {
                return new Future.delayed(const Duration(milliseconds: 1000));
              },
              child: new Container(
                margin: EdgeInsets.all(80.0),
                child: new Column(
                  children: <Widget>[
                    new Image(
                      image: sad,
                      //height: 0.0,
                    ),
                    new Container(
                      child: new Text('No rental'),
                    )
                  ],
                ),
              ));
        } else {
          var body = snapshot.data;
          var count = body['rentals'].length;
          print(count);
          return new Container(
              margin: EdgeInsets.all(10.0),
              child: new RefreshIndicator(
                onRefresh: () async {
                  // GetRent gr = new GetRent();
                  // await gr.getrents();
                  return Future.delayed(new Duration(milliseconds: 1000));
                },
                child: new ListView.builder(
                  itemCount: count == null ? 0 : count,
                  itemBuilder: (BuildContext context, int index) {
                    if (body['rentals'][index]['SERVICE_TYPE'].contains('en')) {
                      top = car;
                      backg = Colors.tealAccent;
                    } else if (body['rentals'][index]['SERVICE_TYPE']
                        .contains('aul')) {
                      top = truck;
                      backg = Colors.yellowAccent;
                    } else {
                      top = new AssetImage('assets/calendar.ico');
                      backg = Colors.lightGreenAccent;
                    }

                    return new GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushNamed('/tripinfo');
                      },
                      child: new Card(
                        //    color: Colors.orange[100],
                        elevation: 2.0,
                        child: new ListTile(
                          onTap: () {
                            AlertDialog a = new AlertDialog(
                              contentPadding: EdgeInsets.all(0.0),
                              content: new Container(
                                child: new Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      color: Colors.orange,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              15,
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Name: ${body['rentals'][index]['NAME']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Contact: ${body['rentals'][index]['CONTACT']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Email: ${body['rentals'][index]['EMAIL']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Service type: ${body['rentals'][index]['SERVICE_TYPE']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Vehicle type: ${body['rentals'][index]['VEHICLE_TYPE']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Pick up point: ${body['rentals'][index]['PICK_UP_POINT']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Destination: ${body['rentals'][index]['DESTINATION']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Start date: ${body['rentals'][index]['START']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'End date: ${body['rentals'][index]['END']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                        'Price: ${body['rentals'][index]['PRICE']}',
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                            showDialog(
                              context: context,
                              child: a,
                            );
                          },
                          onLongPress: () {
                            DateTime d = DateTime.parse(
                                '${body['rentals'][index]['END']}');
                            print(d.difference(DateTime.now()).inDays);
                          },
                          leading: new Image(
                            image: top,
                            color: Colors.orange,
                            height: 35.0,
                          ),
                          trailing: body['rentals'][index]['PAID']
                              ? new Container(
                                  child: new Text(
                                    'Paid',
                                    style: new TextStyle(color: Colors.orange),
                                  ),
                                )
                              : new Container(
                                  child: new Text(
                                    'Pending',
                                    style: new TextStyle(color: Colors.orange),
                                  ),
                                ),
                          title: new Text(
                            '${body['rentals'][index]['PICK_UP_POINT']} - ${body['rentals'][index]['DESTINATION']}',
                            style: new TextStyle(
                                color: Colors.orange,
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: new Text(
                            'End: ${body['rentals'][index]['END']}\nStart Date: ${body['rentals'][index]['START']}',
                            style: new TextStyle(),
                          ),
                          isThreeLine: true,
                        ),
                      ),
                    );
                  },
                ),
              ));
        }
      }),
    );
  }

}