import 'package:flutter/material.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';

class CoachMarks{
   GlobalKey _key;
   CoachMarks(GlobalKey _key){
     this._key = _key;
   }




    void showMark(String text, BoxShape b, {function}) {
    print('CALLED!');
   CoachMark coachMarkTile = CoachMark();
    RenderBox target = _key.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = markRect.inflate(5.0);

    coachMarkTile.show(
        targetContext: _key.currentContext,
        markRect: markRect,
        markShape: b,
        children: [
          Positioned(
             top: markRect.bottom + 15.0,
              right: 5.0,
              child: Text(text,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontStyle: FontStyle.italic,
                    color: Colors.white,
                  )))
        ],
        onClose: function,
        duration: Duration(seconds: 3));
  }

}