import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rydeapp/classes/mappageclass.dart';
import 'package:rydeapp/main.dart';

class GoogleMapImp extends StatefulWidget {
  @override
  _GoogleMapImpState createState() => _GoogleMapImpState();
  final List<double> newSplitLoac;
  final int index;
  final dynamic body;
  GoogleMapImp(
      {@required this.newSplitLoac, @required this.index, @required this.body});
}

class _GoogleMapImpState extends State<GoogleMapImp> {
  @override
  void initState() {
    super.initState();
    setPoly();
  }

  @override
  void dispose() {
    mapController.clearPolylines();
    mapController.clearMarkers();
    super.dispose();
  }

  double setMargin = screenheight - screenheight / 5.3;

  Future<Null> setPoly() async {
    print(widget.body);
    MapPageClass mapPageClass = new MapPageClass.trips(
        start: widget.body['trips'][widget.index]['geoData']['STARTLATLONG'],
        end: widget.body['trips'][widget.index]['geoData']['ENDLATLONG'],
        zoom: 17.0,
        context: context);
    await mapPageClass.showmap();
    LatLng end = mapPageClass.getPoly().removeLast();
    LatLng begin = mapPageClass.getPoly()[0];
    new Timer(new Duration(milliseconds: 500), () {
      mapController.addPolyline(new PolylineOptions(
        startCap: Cap.RoundCap,
        endCap: Cap.RoundCap,
        geodesic: true,
        jointType: JointType.Route,
        visible: true,
        points: mapPageClass.getPoly(),
        clickable: true,
        color: Colors.redAccent.value,
      ));
      mapController.addMarker(new MarkerOptions(
        position: begin,
      ));
      mapController.addMarker(new MarkerOptions(
        position: end
      ));

    }
    
    
    );
  }

  GoogleMapController mapController;
  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        elevation: 12.0,
        title: new Text('Map'),
      ),
      body: new Container(
          child: new Stack(
        fit: StackFit.loose,
        children: <Widget>[
          new GoogleMap(
            trackCameraPosition: true,
            tiltGesturesEnabled: true,
            onMapCreated: _onMapCreated,
            zoomGesturesEnabled: true,
            rotateGesturesEnabled: true,
            scrollGesturesEnabled: true,
            initialCameraPosition: CameraPosition(
                zoom: 11.5,
                target:
                    new LatLng(widget.newSplitLoac[0], widget.newSplitLoac[1])),
            mapType: MapType.normal,
            compassEnabled: true,
            myLocationEnabled: true,
          ),
          new GestureDetector(
            child: new Container(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: new AnimatedContainer(
                margin: EdgeInsets.only(top: setMargin),
                //  padding: EdgeInsets.all(20.0),
                width: MediaQuery.of(context).size.width / 1.05,
                duration: new Duration(milliseconds: 300),
                child: new Card(
                  
                  elevation: 9.0,
                  child: new ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                            'Route: ${widget.body['trips'][widget.index]['ROUTE']}',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.body2),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                          'Morning departure time in: ${widget.body['trips'][widget.index]['MORNING']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                          'Evening departure time in: ${widget.body['trips'][widget.index]['EVENING']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                          'Seats Remaining: ${widget.body['trips'][widget.index]['SEATS']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Container(
                        margin:
                            EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
                        child: new Text(
                          'Zone: ${widget.body['trips'][widget.index]['zone']['NAME']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Container(
                        margin:
                            EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
                        child: new Text(
                          'Car description: ${widget.body['trips'][widget.index]['DESCRIPTION']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Container(
                        margin:
                            EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
                        child: new Text(
                          'Car number: ${widget.body['trips'][widget.index]['CAR_NO']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Container(
                        margin:
                            EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
                        child: new Text(
                          'Loc: ${widget.body['trips'][widget.index]['geoData']['STARTLATLONG']}',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      new Padding(
                        padding: EdgeInsets.only(bottom: 20.0),
                      )
                    ],
                  ),
                ),
              ),
            ),
            onVerticalDragUpdate: (d) {
              print(d);
            },
          ),
          new Positioned(
            bottom: MediaQuery.of(context).size.height / 39,
            right: MediaQuery.of(context).size.width / 17,
            child: new FloatingActionButton(
              elevation: 20.0,
              onPressed: () {
                setState(() {
                  if (setMargin == screenheight - screenheight / 5.3) {
                    setMargin = MediaQuery.of(context).size.height / 2.0;
                  } else {
                    setMargin = screenheight - screenheight / 5.3 ;
                  }
                });
              },
              child: new Icon(
                setMargin == screenheight - screenheight / 5.3
                    ? Icons.arrow_upward
                    : Icons.arrow_downward,
                color: Colors.white,
              ),
            ),
          ),
        ],
      )),
    );
  }
}
