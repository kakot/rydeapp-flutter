import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:rydeapp/Interfaces/home.dart';
import 'package:rydeapp/main.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  static String shuttle =
      ' This is a transport service for everyone (employee pickup, bus shuttles for schools, child pickups). We have pickup stations near you. ';
  static String rental =
      ' This service covers rental of vehicles (saloon cars, four wheel drives and vans). This could be a haulage or a regular vehicle rental. ';
  static String organiser =
      ' Create an event or a trip and allow people to join. Each created trip comes with a report to keep you posted on payments. ';
  final page = [new PageViewModel(
      pageColor: Colors.orangeAccent,
      bubbleBackgroundColor: Colors.black,
      body: Text(
        'RYDE LOGISTICS',
      ),
      title: Text('Welcome ${user.displayName}',textAlign: TextAlign.center,),
      mainImage: Image.network(
        user.photoUrl,
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    new PageViewModel(
      pageColor: Colors.white,
      bubbleBackgroundColor: Colors.black,
      body: Text(
        shuttle,
      ),
      title: Text('Shuttle'),
      mainImage: Image.asset(
        'assets/busImage01.jpg',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    new PageViewModel(
      pageColor: Colors.cyanAccent,
      bubbleBackgroundColor: Colors.black,
      body: Text(
        rental,
      ),
      title: Text('Rental'),
      mainImage: Image.asset(
        'assets/rentcar.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    new PageViewModel(
      pageColor: Colors.amberAccent,
      bubbleBackgroundColor: Colors.black,
      body: Text(
        organiser,
      ),
      title: Text('Organizer'),
      mainImage: Image.asset(
        'assets/orgbus.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Builder(
        builder: (context) => IntroViewsFlutter(
              page,
              onTapDoneButton: () {
                Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) => new Home(0)));
              },
              onTapSkipButton: () {
                Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) => new Home(0)));
              },
              showSkipButton:
                  true, //Whether you want to show the skip button or not.
              pageButtonTextStyles: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
              ),
            ),
      ),
    );
  }
}
