import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => new _AboutPageState();
}


String welcomcontent = "Ryde Logistics Limited is a service-oriented company established in 2016, that aims at providing convenient and reliable transport solutions to its customers. Our dream is to help you get things done easily – in terms of transport that is.";


String whatcontent = 'As a Transport Logistics company, we reduce the stress you go through in having to search, negotiate and organize people and transportation for your various needs as RYDE does all that for you. Tell us WHAT you want and WHEN you want it and we’ll bring it to you. Just give us at least a two (2) days notice. Terms and conditions apply.';
var logo = new AssetImage('assets/rrydelogo.jpg');
class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context){
    return new Scaffold(
      body: new Center(
        child: new ListView(
          children: <Widget>[
            new Container(
              child: new Image(
                image: logo,
                 height: MediaQuery.of(context).size.height/4.5,
              ),
            ),
              new Container(
                margin: EdgeInsets.only(
                  left: 30.0,right: 30.0,
                ) ,
                child: new Text('Welcome To Ryde',textAlign: TextAlign.center ,style: new TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),),
              ),
              new Container(
                margin: EdgeInsets.only(
                  top: 5.0,left: 30.0,right: 30.0,bottom: 10.0,
                ) ,
                child: new Text(welcomcontent,textAlign: TextAlign.center ,style: new TextStyle(
                  fontSize: 17.0,
                ),),
              ),
              new Container(
                margin: EdgeInsets.only(
                  top: 20.0,left: 30.0,right: 30.0,bottom: 10.0,
                ) ,
                child: new Text('What We Do',textAlign: TextAlign.center ,style: new TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),),
              ),
              new Container(
                margin: EdgeInsets.only(
                  top: 5.0,left: 30.0,right: 30.0,bottom: 10.0,
                ) ,
                child: new Text(whatcontent,textAlign: TextAlign.center ,style: new TextStyle(
                  fontSize: 17.0,
                ),),
              ),
              new Container(
                 child: new FlatButton(
                    child: new Text('View privacy policy', style: new TextStyle(
                       color: Colors.orange
                    ),),
                     onPressed: (){
                      String url = 'https://rydelogistics.firebaseapp.com/ryde/privacy';
                        launch(url);
                     },
                 ),
              ),
          ],
        ),
      ) ,
    );
  }
}
