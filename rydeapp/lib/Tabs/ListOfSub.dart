import 'package:flutter/material.dart';
import 'package:rydeapp/customWidgets.dart';

class ListSub extends StatefulWidget {
  @override
  _ListSubState createState() => _ListSubState();
  final Map<String, dynamic> list;
  ListSub(this.list);
}

class _ListSubState extends State<ListSub> {
  @override
  Widget build(BuildContext context) {
    var data = widget.list['data'];
    int count = data.length;
    print(count);
    return new Scaffold(
        appBar: appbar('Subscribers', context, '/aboutpage'),
        body: new Container(
          margin: EdgeInsets.all(10.0),
          child: new ListView.builder(
            itemCount: data.length == 0 ? 1 : data.length,
            itemBuilder: ((context, index) {
              DateTime a;
               a = DateTime.parse(data[index]['user_org']['updatedAt']).toLocal();
              String r_time = '${a.year}/${a.month}/${a.day} at ${a.hour}:${a.minute} ${a.timeZoneName}';
              if (data.length == 0) {
                print('here');
                return new Column(
                   children: <Widget>[
                      new Container(
                   margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height/10,
                   ),
                  child: new ClipOval(
                    child: new Image(
                      image: new AssetImage('assets/tbus.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                new Container(
                   child: new Text('No Subscribers'),
                )
                   ],
                );
              } else {
                return new ListTile(
                   leading: new Text('${index+1}.', style: new TextStyle(

                      color: Colors.orange,
                       fontSize: 20.0
                   ),),
                  dense: true,
                  contentPadding: EdgeInsets.all(3.0),
                  isThreeLine: true,
                  subtitle: new Text(
                      'Email: ${data[index]['EMAIL']}\nPhone number: ${data[index]['PHONE_NUMBER']}\nPaid on: $r_time'),
                  trailing:
                      new Text('${data[index]['user_org']['SEAT_NO']} seat(s)'),
                  title: new Text(
                    'Name: ${data[index]['FULL_NAME']}',
                    style: new TextStyle(color: Colors.orange),
                  ),
                );
              }
            }),
          ),
        ));
  }
}
