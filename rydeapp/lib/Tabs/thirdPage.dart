import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/Tabs/ListOfSub.dart';
import 'package:rydeapp/Tabs/ThirdpageSections/RentalSection.dart';
import 'package:rydeapp/Tabs/ThirdpageSections/TripsSection.dart';
import 'package:rydeapp/Tabs/ThirdpageSections/organiser.dart';
import 'package:rydeapp/classes/CoachMarks.dart';
import 'package:rydeapp/classes/http/ThirdPageHttp.dart';
import 'package:rydeapp/classes/qrimage.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:rydeapp/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rydeapp/classes/http/Subscribers.dart';
import 'package:share/share.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
  final int initPage;
  ThirdPage(this.initPage);
}

class _ThirdPageState extends State<ThirdPage> {
  AssetImage sad = new AssetImage('assets/tbus.jpg');

 
RentalSection r = new RentalSection();
TripsSection t = new TripsSection();
  int start;
  var car = AssetImage('assets/sedan.ico');
  var haullogo = AssetImage('assets/truck.jpg');
  var eventlogo = AssetImage('assets/event.jpg');

  TabController tabController =
      new TabController(vsync: AnimatedListState(), length: 3, initialIndex: 0);

  @override
  void initState() {
    start = widget.initPage;

    WidgetsBinding.instance.addPostFrameCallback((_) => showCoach());

    // TODO: implement initState
    super.initState();
  }

  GlobalKey _profileKey = new GlobalObjectKey('profile');
  GlobalKey _logoutKey = new GlobalObjectKey('logout');
  GlobalKey _aboutKey = new GlobalObjectKey('about');
  void showCoach() {
    final String _aboutCoach = 'You can find out more about us clicking here';
    final String _logoutCoach = 'click here to logout';
    final String _profileCoach = 'Click here to view your profile info';
    Timer t = new Timer(new Duration(milliseconds: 300), () {
      CoachMarks cm = new CoachMarks(_profileKey)
        ..showMark(_profileCoach, BoxShape.circle, function: () {
          CoachMarks cm = new CoachMarks(_aboutKey)
            ..showMark(_aboutCoach, BoxShape.circle, function: () {
              CoachMarks cm = new CoachMarks(_logoutKey)
                ..showMark(_logoutCoach, BoxShape.circle);
            });
        });
    });
  }

  bool isorgloading = false;
  OrgSection o = new OrgSection();
  GlobalKey<ScaffoldState> _scaff = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        key: _scaff,
        appBar: new AppBar(
          elevation: 0.2,
          bottom: new TabBar(
            //  isScrollable: true,
            controller: tabController,
            tabs: <Widget>[
              new Container(
                child: new Tab(
                  child: new Text(
                    'Shuttle',
                    style: new TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                ),
              ),
              new Container(
                child: new Tab(
                  child: new Text(
                    'Rentals',
                    style: new TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                ),
              ),
              new Container(
                child: new Tab(
                  child: new Text(
                    'Organized Trip',
                    style: new TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                ),
              ),
            ],
          ),
          title: new Text(
            'Dashboard',
            style: new TextStyle(color: Colors.black),
            textAlign: TextAlign.center,
          ),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new IconButton(
              key: _profileKey,
              icon: new Icon(Icons.account_circle),
              onPressed: () {
                Navigator.of(context).pushNamed('/profilepage');
              },
            ),
            new IconButton(
              key: _aboutKey,
              tooltip: 'About Us',
              icon: new Icon(Icons.info_outline),
              color: Colors.black,
              onPressed: () {
                Navigator.of(context).pushNamed('/aboutpage');
              },
            ),
            new IconButton(
              key: _logoutKey,
              tooltip: 'Logout',
              icon: new Icon(Icons.exit_to_app),
              color: Colors.black,
              onPressed: () async {
                Future<SharedPreferences> checklogin =
                    SharedPreferences.getInstance();

                Future<Null> storelogin() async {
                  final SharedPreferences loginpref = await checklogin;
                  loginpref.setString('token', '');
                }

                await storelogin();
                Navigator.of(context).pushReplacementNamed('/splash');
              },
            ),
          ],
        ),
        body: new TabBarView(
          controller: tabController,
          children: <Widget>[
            new Container(child: t.tripstab(car,_scaff,bus,sad)),
            new Container(
              child: r.rentalpage(sad,car,truck),
            ),
            new Container(
              child: o.orgpage(_scaff, bus, sad, (me) {
                setState(() {
                  me = !me;
                });
              }),
            )
          ],
        ));
  }
}
