import 'dart:async';

import 'package:flutter/material.dart';
class DashboardBuilder extends StatefulWidget {
  @override
  _DashboardBuilderState createState() => new _DashboardBuilderState();
  final ImageProvider ic;
  final Map<String, dynamic> body;
  final int index, count;
  final Function function1,function2,function3,function4;
  DashboardBuilder(this.body, this.ic, this.index, this.count, {this.function1,this.function2,this.function3,this.function4});
}

class _DashboardBuilderState extends State<DashboardBuilder> {
  GlobalKey expandedCard = new GlobalObjectKey('Theexpandedcard');
  List<GlobalKey> lists = [];
  Size cardSize;
  getSizeofCard() {
    RenderBox expandedCardRenderbox =
        lists[widget.index].currentContext.findRenderObject();
    cardSize = expandedCardRenderbox.size;
    print(cardSize);
  }

  populateKey() {
    for (int i = 0; i <= widget.count; i++) {
      lists.add(new GlobalObjectKey('$i'));
    }
  }

  @override
  void initState() {
    print(widget.body);
    super.initState();
    populateKey();
   // WidgetsBinding.instance.addPostFrameCallback(_afterlayout);
  }

  _afterlayout(_) {
    Timer t = new Timer(new Duration(milliseconds: 3000), () {
      getSizeofCard();
    });
  }

  double wi = 0.0;
  bool isExpanded = true;
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        // top: 0.5,
        // bottom: 0.5
      ),
      child: new Column(
        children: <Widget>[
          new Card(
              elevation: 3.0,
              child: new Container(
                  margin: EdgeInsets.all(10.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Container(
                        width: MediaQuery.of(context).size.width / 11,
                        child: new Image(
                          color: Colors.orange,
                          image: widget.ic,
                          fit: BoxFit.contain,
                        ),
                      ),
                      new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          // new Container(
                          //   child: new Text(
                          //     'Destination: Aflao',
                          //     style: new TextStyle(
                          //         fontSize: 15.0, color: Colors.orange),
                          //   ),
                          // ),
                          new Padding(
                            padding: EdgeInsets.all(5.0),
                          ),
                          new Container(
                            width: MediaQuery.of(context).size.width / 2,
                            child: new Text(
                              '${widget.body['ROUTE']}',
                              style: new TextStyle(
                                  fontSize: 15.0, color: Colors.orange),
                            ),
                          )
                        ],
                      ),
                      new IconButton(
                        icon: new Icon(isExpanded
                            ? Icons.arrow_downward
                            : Icons.arrow_upward),
                        onPressed: () {
                          if (isExpanded) {
                            setState(() {
                           //   print(cardSize.height);
                              wi = 250 + 10.0;
                              isExpanded = !isExpanded;
                            });
                          } else {
                            setState(() {
                              wi = 0.0;
                              isExpanded = !isExpanded;
                            });
                          }
                        },
                      )
                    ],
                  ))),
          new Card(
            key: lists[widget.index],
            child: new AnimatedContainer(
                color: Colors.grey[100],
                duration: new Duration(milliseconds: 300),
                curve: Curves.ease,
                height: wi,
                width: double.infinity,
                child: new Container(
                    margin: EdgeInsets.all(10.0),
                    child: new ListView(
                      
                      shrinkWrap: true,
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.all(10.0),
                          child: new Text(
                            'Morning Departure Time: ${widget.body['MORNING']}',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.all(10.0),
                          child: new Text(
                            'Evening Departure Time: ${widget.body['EVENING']}',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.all(10.0),
                          child: new Text(
                            'Seats checked out: ${widget.body['user_trip']['SEATS']}',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.all(10.0),
                          child: new Text(
                            'Car description: ${widget.body['DESCRIPTION']}',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.all(10.0),
                          child: new Text(
                            'Car number: ${widget.body['CAR_NO']}',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                        ),
                        new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              new Container(
                                child: new OutlineButton(
                                color: Colors.white,
                                
                                  borderSide: new BorderSide(
                                    color: Colors.orange
                                  ),
                                  onPressed: () {
                                    widget.function2();
                                  },
                                  child: new Text('View Ticket'),
                                ),
                              ),
                              new Container(
                                child: new OutlineButton(
                                   borderSide: new BorderSide(
                                    color: Colors.orange
                                  ),
                                  color: Colors.orange,
                                  onPressed: () {
                                    widget.function1('hi');
                                  },
                                  child: new Text('Pay'),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ))),
          ),
        ],
      ),
    );
  }
}
