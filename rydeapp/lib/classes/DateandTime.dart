import 'dart:async';

import 'package:flutter/material.dart';



class DateandTime  {
  DateandTime();


  DateTime date = new DateTime.now();
  TimeOfDay time = new TimeOfDay.now();
  Widget altdandt(BuildContext context) {
     return dateandtime(context, 'Time and Date: ');
  }

  Future<Null> selectdate(BuildContext context) async {
    var datee = await showDatePicker(
        lastDate: new DateTime(2019),
        firstDate: new DateTime(2016),
        initialDate: date,
        context: context);
    if (datee != null && datee != date) {
      date = datee;
    }

    var timee = await showTimePicker(context: context, initialTime: time);
    if (timee != null && timee != time) {
      time = timee;
    }
  }

  Widget dateandtime(BuildContext context, String message) {
    double toppadding = 30.0;
    double downpadding = 30.0;
    double leftpadding = 10.0;
    String t = '${date.year}-${date.month}-${date.day}';
    return new Container(
      child: new Container(
        margin: EdgeInsets.only(top: toppadding),
        child: new FlatButton(
          child: new Row(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                margin: EdgeInsets.only(bottom: downpadding),
                child: new Icon(
                  Icons.date_range,
                  color: Colors.orange,
                ),
              ),
              new Container(
                margin: EdgeInsets.only(left: leftpadding, bottom: downpadding),
                child: new Text(
                  message,
                  style: new TextStyle(fontSize: 20.0, color: Colors.orange),
                ),
              ),
              new Container(
              //  width: 150.0,
                padding: EdgeInsets.all(leftpadding),
                decoration: new BoxDecoration(
                  border: Border.all(color: Colors.orange),
                ),
                margin: EdgeInsets.only(left: leftpadding, bottom: downpadding),
                child: new Text(t, style: new TextStyle(fontSize: 20.0,))
              ),
            ],
          ),
          onPressed: () {
            selectdate(context);
          },
        ),
      ),
    );
  }
}
