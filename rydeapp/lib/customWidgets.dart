import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rydeapp/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

var head = new AssetImage('assets/danfoman.jpg');

var truck = new AssetImage('assets/delivery-truck.ico');

var car = new AssetImage('assets/sedan.ico');

var calendar = new AssetImage('assets/calendar.ico');

var bus = new AssetImage('assets/bus.png');

Widget textf(String d, example, TextEditingController t) {
  return new Container(
    padding: EdgeInsets.only(left: 10.0),
    margin: EdgeInsets.only(left: 18.0, right: 18.0, top: 10.0, bottom: 10.0),
    child: new TextField(
      controller: t,
      decoration: new InputDecoration(labelText: d, hintText: example),
    ),
  );
}

Widget droppd(String i) {
  return new DropdownMenuItem(
    child: new Container(
      margin: EdgeInsets.only(
        left: 25.0,
      ),
      width: 245.0,
      child: new Text(i),
    ),
  );
}


Widget cardmaker(List<String> s) {
  return new Container(
    child: new Card(
        elevation: 6.0,
        child: new Container(
          margin: EdgeInsets.only(top: 20.0),
          height: 200.0,
          child: new ListView.builder(
            controller: new ScrollController(keepScrollOffset: false),
            scrollDirection: Axis.vertical,
            itemCount: s.length,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return new Container(
                  child: new Text(
                    s[index],
                    style: new TextStyle(fontSize: 20.0),
                  ),
                );
              }
              return new Container(
                  child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    width: 0.87 * MediaQuery.of(context).size.width,
                    margin: EdgeInsets.all(10.0),
                    child: new Text(
                      s[index],
                      textAlign: TextAlign.start,
                    ),
                  )
                ],
              ));
            },
          ),
        )),
  );
}

Widget ccardmaker(GlobalKey<ScaffoldState> s) {
  return new Container(
    height: 50.0,
    child: new RaisedButton(
      color: Colors.orange,
      child: new Text('Give us a feedback'),
      onPressed: () {
        s.currentState.showBottomSheet((context) {
          return new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Container(
                child: new TextFormField(
                  decoration: new InputDecoration(labelText: 'Name:'),
                ),
              ),
              new Container(
                child: new TextFormField(
                  decoration: new InputDecoration(labelText: 'Comment:'),
                ),
              )
            ],
          );
        });
      },
    ),
  );
}

Widget rentalcard(String title, String content, ImageProvider ic,
    BuildContext context, String goto, double swidth, Widget wid) {
  double toppadding = 20.0;
  double bottompadding = 10.0;
  return new Container(
      margin: EdgeInsets.only(bottom: bottompadding * 2),
      child: new Card(
        elevation: 3.0,
        child: new Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Container(
                margin: EdgeInsets.only(top: toppadding, bottom: bottompadding),
                child: new Text(
                  title,
                  textAlign: TextAlign.left,
                  style: new TextStyle(fontSize: 25.0),
                ),
              ),
              // new Container(
              //   child: new Image(
              //     height: 80.0,
              //     image: ic,
              //   ),
              //),
              new Container(
                margin: EdgeInsets.only(
                    left: 5.0,
                    right: 5.0,
                    top: toppadding,
                    bottom: bottompadding),
                child: new Text(
                  content,
                  style: new TextStyle(fontSize: 18.0),
                ),
              ),
              wid,
            ],
          ),
        ),
      ));
}

Widget appbar(String title, BuildContext context, String goto,
    {bool showProf = true}) {
  var t = new AppBar(
    elevation: 0.0,
    title: new Text(
      title,
      style: new TextStyle(
        color: Colors.black,
      ),
      textAlign: TextAlign.center,
    ),
    backgroundColor: Colors.white,
    actions: <Widget>[
      showProf
          ? new IconButton(
              icon: new Icon(Icons.account_circle),
              onPressed: () {
                Navigator.of(context).pushNamed('/profilepage');
              },
            )
          : new Container(),
      new IconButton(
        tooltip: 'About Us',
        color: Colors.black,
        icon: new Icon(Icons.info_outline),
        onPressed: () {
          Navigator.of(context).pushNamed(goto);
        },
      ),
      new IconButton(
        tooltip: 'Logout',
        color: Colors.black,
        icon: new Icon(Icons.exit_to_app),
        onPressed: () async {
          Future<SharedPreferences> checklogin =
              SharedPreferences.getInstance();

          Future<Null> storelogin() async {
            final SharedPreferences loginpref = await checklogin;
            loginpref.setString('token', '');
            token = null;
          }

          await storelogin();
          Navigator.of(context).pushReplacementNamed('/splash');
        },
      ),
    ],
  );

  return t;
}

String stat = 'Pending';
String source = 'Accra';
String destination = 'Circle';
Widget tripsforminfo(String title, String info) {
  return new Container(
      margin: EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(right: 10.0),
            child: new Text(
              title,
              style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
            ),
          ),
          new Text(info),
        ],
      ));
}
