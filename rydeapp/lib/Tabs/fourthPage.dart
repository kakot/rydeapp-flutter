import 'package:flutter/material.dart';
import 'package:rydeapp/Interfaces/FeedbackScreen.dart';
import 'package:rydeapp/classes/http/PostFeedback.dart';
import 'package:rydeapp/customWidgets.dart';

class FourthPage extends StatefulWidget {
  @override
  _FourthPageState createState() => _FourthPageState();
}

class _FourthPageState extends State<FourthPage> {
  List<String> internet = ['ok', 'Hi', 'here'];
  var backg = new AssetImage('assets/banner2.jpg');
  String content =
      'Let’s get down to payment shall we. Very Important. We have different fare types depending on what you are comfortable with.';
  var qnaicon = new AssetImage('assets/question.png');

  List<String> internetFares = [
    'Internet Fares',
    '• Part refund. A penalty of 10% of the original amount will be charged.',
    '• Process payment via online form',
    '• Pay with either mobile wallet; domestic or \n  international card',
  ];

  List<String> noninternetFares = [
    'Non-Internet Fares',
    '• No refund',
    '• Confirm and receive booking Reference Number',
    '• Present Reference Number to Booking Manager',
    '• Conclude and confirm payment'
  ];

  List<String> refund = [
    'You may get a refund if:',
    '• You book another ride date and time within the \n  same day of booking first ride',
    '• Bus is more than 30 minutes late (unfortunately, \n  things like mechanical damage, traffic, weather \n  etc can affect arrival and departure times)'
  ];

  List<String> norefund = [
    'No/Part Refund if:',
    '• You completely cancel a booking and request \n  for a refund. Amount less 10% of original will be \n  refunded',
    '• You fail to take booked ride and then request \n  for a refund after booking trip.',
  ];

  TextEditingController _message = new TextEditingController();
  TextEditingController _title = new TextEditingController();

  GlobalKey<ScaffoldState> _scaff = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaff,
      appBar: appbar('Help & Information', context, '/aboutpage'),
      body: new Container(
          child: new ListView(
        children: <Widget>[
          new Container(
              height: 300.0,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: backg,
                ),
              ),
              child: new Container(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image(
                      image: qnaicon,
                      height: 70.0,
                      color: Colors.white,
                    ),
                    new Container(
                      margin:
                          EdgeInsets.only(left: 10.0, right: 10.0, top: 30.0),
                      child: new Text(
                        content,
                        textAlign: TextAlign.center,
                        style:
                            new TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 30.0),
                      child: new Text(
                        'Help and Info',
                        style: new TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              )),
          cardmaker(internetFares),
          cardmaker(noninternetFares),
          cardmaker(refund),
          cardmaker(norefund),
          new Container(
            height: 50.0,
            child: new RaisedButton(
              color: Colors.orange,
              child: new Text('Give us a feedback'),
              onPressed: () {
                Navigator.of(context).push(new MaterialPageRoute(
                   builder: ((b){
                     return new FeedBackScreen();
                   })
                ));
              },
            ),
          )
        ],
      )),
    );
  }
}

class Feed {
  postFeed() {}
}
