import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';

class GetSubcribers {
  var client = new http.Client();

  getSub(int id) async {
     Map<String,dynamic>body;
    try {
      await client
          .get('https://ryde-rest.herokuapp.com/subscribers/$id', headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': token,
      }).then((res){
        body = json.decode(res.body);
      });
    } catch (e) {}
    return body;
  }
}
