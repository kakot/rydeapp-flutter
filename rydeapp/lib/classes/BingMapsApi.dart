import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';



class BingMapsApi{
String apikey;
String directionsurl;
String initialposition;
String finalposition;
Map<String,dynamic> cord;

  BingMapsApi(String apikey){
    this.apikey = apikey;
  }

final String apiKey = 'AlttOIj3OoyyPhflwbhGOlz3hkuW3QLP2pN5k_-pDTErmXvhSYDxUSsW8NCGd50f';


  Future<List<dynamic>> polyCord(String initialposition,String finalposition) async{
    var client = new http.Client();
        directionsurl = 'http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0=$initialposition&wp.1=$finalposition&routeAttributes=routePath&key=AlttOIj3OoyyPhflwbhGOlz3hkuW3QLP2pN5k_-pDTErmXvhSYDxUSsW8NCGd50f';
    var response = await client.get(directionsurl);
    cord = json.decode(response.body);
    return cord['resourceSets'][0]['resources'][0]['routePath']['line']['coordinates'];
  }

  Future<String> bingGeocode(double lat_position, double long_position)async{
    String url = 'http://dev.virtualearth.net/REST/v1/Locations/${lat_position.toString()},${long_position.toString()}?o=json&key=${this.apikey}';
    var client = new http.Client();
    var response = await client.get(url);
    var cord = json.decode(response.body);
   // print(cord['resourceSets'][0]['resources'][0]['name']);
    return cord['resourceSets'][0]['resources'][0]['name'];
  }

}
