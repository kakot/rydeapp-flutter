import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';
import 'package:url_launcher/url_launcher.dart';

List<String> steps = [
  '1. First Click the PAY NOW button',
  '2. Select either mobile payment/credit card union',
  '3. If you select credit card option, fill your credit card details',
  '4. If you select mobile money payment/select your wallet(MTN mobile money, Vodafone cash, Airtel money,Tigo cash)'
      '5. Now fill the details and make payment',
  '6. Kindly make sure not to delay with payment methods, as token might expire',
  'Hint: If you are on MTN mobile money, kindly allow CASH OUT first'
];

class Payscreen extends StatefulWidget {
  final String price;

  final String clientref;

  final String description;

  final String duration;

  final String item;

  final String paymentType;

  final String tripid;

  final String seatN;

  final String seats;

  final bool fromTrips;

  Payscreen(
      {this.price,
      this.clientref,
      this.item,
      this.description,
      this.paymentType,
      this.tripid,
      this.duration,
      this.seatN,
      this.seats,
      this.fromTrips});
  @override
  _Payscreen createState() => new _Payscreen();
}

class _Payscreen extends State<Payscreen> {
  bool isloading = false;

  final _formK = GlobalKey<FormState>();
  TextEditingController seats = new TextEditingController();

  final key = new GlobalKey<ScaffoldState>();
  var backg = new AssetImage('assets/sims.jpg');
  var payimage = new AssetImage('assets/paybutton.png');
  String iniprice;
  String iniorgprice;
  @override
  void initState() {
    super.initState();
    iniprice = prices.elementAt(0);
    iniorgprice = orgprice.elementAt(0);
  }

  String price;

  void _onchangeprice(String value) {
    setState(() {
      iniprice = value;
    });
  }

  void _onchangeoprice(String value) {
    setState(() {
      iniorgprice = value;
    });
  }

  bool isTrip = false;
  bool isOrg = false;

  double leftpadding = 10.0;
  @override
  Widget build(BuildContext context) {
    if (widget.description.contains('Trip')) {
      setState(() {
        isTrip = true;
      });
    } else if (widget.description.contains('gani')) {
      setState(() {
        isOrg = true;
      });
    }
    return new Scaffold(
        key: key,
        appBar: appbar('Make Payment', context, '/aboutpage'),
        body: Builder(
          builder: (context) {
            return new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: backg,
                  fit: BoxFit.cover,
                ),
              ),
              //margin: EdgeInsets.all(10.0),
              child: new Container(
                height: MediaQuery.of(context).size.height,
                decoration: new BoxDecoration(
                  color: Colors.orange.withOpacity(0.4),
                ),
                child: new ListView(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.only(top: 10.0),
                      child: new Text('Steps For Making Payment',
                          style: new TextStyle(
                              fontSize: 20.0, color: Colors.white)),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: payimage,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      height: 60.0,
                    ),
                    new Container(
                        child: new Card(
                            margin: EdgeInsets.all(10.0),
                            color: Colors.white.withOpacity(0.9),
                            elevation: 10.0,
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                ),
                                step(steps[0]),
                                step(steps[1]),
                                step(steps[2]),
                                step(steps[3]),
                                step(steps[4]),
                                step(steps[5]),
                                new Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                ),
                                //step(steps[6]),
                                isTrip
                                    ? new Container(
                                        decoration: new BoxDecoration(
                                            border: Border.all(
                                                color: Colors.orange,
                                                width: 2.0)),
                                        child: new Card(
                                          child: new Column(
                                            children: <Widget>[
                                              new Container(
                                                  margin: EdgeInsets.only(
                                                      left: 10.0, right: 10.0),
                                                  child: new Row(
                                                    children: <Widget>[
                                                      new Container(
                                                        child: new Text(
                                                          'Please Select Option:     ',
                                                          style: new TextStyle(
                                                              color: Colors.red,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      new DropdownButton(
                                                        onChanged:
                                                            (dynamic value) {
                                                          _onchangeprice(value);
                                                        },
                                                        value: iniprice,
                                                        items: prices.map(
                                                            (String value) {
                                                          return new DropdownMenuItem(
                                                            value: value,
                                                            child:
                                                                new Container(
                                                              //width: screenwidth * 0.5,
                                                              child: new Text(
                                                                value,
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        12.0,
                                                                    color: Colors
                                                                        .red),
                                                              ),
                                                            ),
                                                          );
                                                        }).toList(),
                                                      )
                                                    ],
                                                  )),
                                              widget.fromTrips
                                                  ? Form(
                                                      key: _formK,
                                                      child: new Container(
                                                        margin: EdgeInsets.only(
                                                          left: leftpadding,
                                                          right:
                                                              leftpadding * 3,
                                                          //bottom: leftpadding
                                                        ),
                                                        child:
                                                            new TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          validator: (val) {
                                                            if (val.isEmpty) {
                                                              return 'Please Input Seat';
                                                            } else if (int
                                                                    .parse(
                                                                        val) >
                                                                int.parse(
                                                                    '${widget.seats}')) {
                                                              return '${widget.seats} Remaining';
                                                            } else {
                                                              return null;
                                                            }
                                                          },
                                                          controller: seats,
                                                          decoration: new InputDecoration(
                                                              labelText:
                                                                  'Seat: ',
                                                              labelStyle:
                                                                  new TextStyle(
                                                                      color: Colors
                                                                          .orange)),
                                                        ),
                                                      ),
                                                    )
                                                  : new Container()
                                            ],
                                          ),
                                        ),
                                      )
                                    : new Container(),
                                isOrg
                                    ? new Container(
                                        margin: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                        child: new Row(
                                          children: <Widget>[
                                            new Container(
                                              child: new Text(
                                                'Please Select Option:     ',
                                                style: new TextStyle(
                                                    color: Colors.red,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            new DropdownButton(
                                              onChanged: (dynamic value) {
                                                _onchangeoprice(value);
                                              },
                                              value: iniorgprice,
                                              items:
                                                  orgprice.map((String value) {
                                                return new DropdownMenuItem(
                                                  value: value,
                                                  child: new Container(
                                                    //width: screenwidth * 0.5,
                                                    child: new Text(
                                                      value,
                                                      style: new TextStyle(
                                                          fontSize: 12.0,
                                                          color: Colors.red),
                                                    ),
                                                  ),
                                                );
                                              }).toList(),
                                            )
                                          ],
                                        ))
                                    : new Container(),
                                new Container(
                                  //  flex:1 ,
                                  child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: MediaQuery.of(context).size.height *
                                        0.08,
                                    child: isloading
                                        ? new Center(
                                            child: CircularProgressIndicator(),
                                          )
                                        : new RaisedButton(
                                            onPressed: () async {
                                              bool valid;
                                              try {
                                                valid = _formK.currentState
                                                    .validate();
                                              } catch (e) {
                                                seats.text = widget.seatN;
                                                valid = !widget.fromTrips;
                                              }

                                              if (widget.description
                                                      .contains('Trip') &&
                                                  valid) {
                                                PostPrice postPrice =
                                                    new PostPrice(
                                                        seatN: seats.text,
                                                        description:
                                                            widget.description,
                                                        duration:
                                                            widget.duration,
                                                        clientref:
                                                            widget.clientref,
                                                        item: widget.item,
                                                        paymentType:
                                                            widget.paymentType,
                                                        price: iniprice,
                                                        tripid: widget.tripid);

                                                setState(() {
                                                  isloading = true;
                                                });

                                                await postPrice.postprice(
                                                    context, isloading);

                                                setState(() {
                                                  isloading = false;
                                                });
                                              } else if (widget.description
                                                  .contains('ganizer')) {
                                                print('Organizer!');
                                                PostPrice postPrice =
                                                    new PostPrice(
                                                  seatN: 1.toString(),
                                                  description:
                                                      widget.description,
                                                  duration: widget.duration,
                                                  clientref: widget.clientref,
                                                  item: widget.item,
                                                  paymentType:
                                                      widget.paymentType,
                                                  price: iniorgprice,
                                                );

                                                setState(() {
                                                  isloading = true;
                                                });

                                                await postPrice.postOrgprice(
                                                    context, isloading);

                                                setState(() {
                                                  isloading = false;
                                                });
                                              } else if (widget.description
                                                  .contains('ent')) {
                                                PostPrice postPrice =
                                                    new PostPrice(
                                                        seatN: widget.seatN,
                                                        description:
                                                            widget.description,
                                                        duration:
                                                            widget.duration,
                                                        clientref:
                                                            widget.clientref,
                                                        item: widget.item,
                                                        paymentType:
                                                            widget.paymentType,
                                                        price: widget.price,
                                                        tripid: widget.tripid);

                                                setState(() {
                                                  isloading = true;
                                                });

                                                await postPrice.postpricerent(
                                                    context, isloading);

                                                setState(() {
                                                  isloading = false;
                                                });
                                              }
                                            },
                                            child: new Text('PAY NOW'),
                                            color: Colors.orangeAccent,
                                          ),
                                  ),
                                ),
                              ],
                            ))),
                  ],
                ),
              ),
            );
          },
        ));
  }

  Widget step(String t) {
    return new Container(
      margin: EdgeInsets.only(
        top: 8.0,
        left: 10.0,
        right: 10.0,
        bottom: 2.0,
      ),
      child: new Text(
        t,
        style: new TextStyle(
          fontSize: 15.0,
          color: Colors.black,
          //fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class PostPrice {
  String price;

  String clientref;

  String description;

  String duration;

  String item;

  String paymentType;

  String seatN;

  String tripid;
  PostPrice(
      {this.price,
      this.clientref,
      this.item,
      this.description,
      this.paymentType,
      this.tripid,
      this.duration,
      this.seatN});
  var client = new http.Client();
  Future postprice(BuildContext context, bool isLoading) async {
    isLoading = true;
    if (this.price.contains('200')) {
      this.price = '200.00';
      this.duration = 'monthly';
    } else if (this.price.contains('40')) {
      this.price = '40.00';
      this.duration = 'twoweeks';
    } else if (this.price.contains('3')) {
      this.price = '3.00';
      this.duration = 'threedays';
    } else {
      this.price = '20.00';
      this.duration = 'weekly';
    }
    await client.post('https://ryde-rest.herokuapp.com/checkout', headers: {
      'Accept': "application/json, text/plain, */*",
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    }, body: {
      "item": this.item.toString(),
      "price": this.price.toString(),
      'description': this.description,
      'payment_type': this.paymentType,
      'duration': this.duration,
      'tripId': this.tripid,
      'seatNo': this.seatN
    }).then((res) {
      isLoading = false;
      Map<String, dynamic> body = json.decode(res.body);
      if (body['status'] == 'success') {
        // GetRent getRent = new GetRent();
        // GetOrganised getOrganised = new GetOrganised();
        // GetTrip getTrip = new GetTrip();

        // getOrganised.getorganised();
        // getTrip.gettrips();
        // getRent.getrents();
        launch(body['checkoutUrl']);
      } else if (body['status'] == 'failed') {
        final snackBar = SnackBar(
          duration: new Duration(milliseconds: 7000),
          content: Text(body['checkoutUrl']['data']),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      }
    });
  }

  Future postOrgprice(BuildContext context, bool isLoading) async {
    isLoading = true;
    if (this.price.contains('80')) {
      this.price = '80.00';
      this.duration = '7 days';
    } else if (this.price.contains('150')) {
      this.price = '150.00';
      this.duration = '14';
    } else if (this.price.contains('200')) {
      this.price = '200';
      this.duration = '21 days';
    } else {
      this.price = '300';
      this.duration = '1 month';
    }
    await client.post('https://ryde-rest.herokuapp.com/checkout', headers: {
      'Accept': "application/json, text/plain, */*",
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    }, body: {
      "item": this.item.toString(),
      "price": this.price.toString(),
      'description': this.description,
      'payment_type': this.paymentType,
      'period': this.duration,
      'seatNo': this.seatN,
      'clientReference': this.clientref,
    }).then((res) {
      isLoading = false;
      Map<String, dynamic> body = json.decode(res.body);
      if (body['status'] == 'success') {
        // GetRent getRent = new GetRent();
        // GetOrganised getOrganised = new GetOrganised();
        // GetTrip getTrip = new GetTrip();

        // getOrganised.getorganised();
        // getTrip.gettrips();
        // getRent.getrents();
        launch(body['checkoutUrl']);
      } else if (body['status'] == 'failed') {
        final snackBar = SnackBar(
          duration: new Duration(milliseconds: 7000),
          content: Text(body['checkoutUrl']['data']),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      }
    });
  }

  Future postpricerent(BuildContext context, bool isLoading) async {
    isLoading = true;
    await client.post('https://ryde-rest.herokuapp.com/checkout', headers: {
      'Accept': "application/json, text/plain, */*",
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    }, body: {
      "item": this.item.toString(),
      "price": this.price.toString(),
      'description': this.description,
      'clientReference': this.clientref == null ? 'undefined' : this.clientref,
      'payment_type': this.paymentType,
      'duration': this.duration == null ? 'undefined' : this.duration,
      'seatNo': this.seatN == null ? 'undefined' : this.seatN
    }).then((res) {
      isLoading = false;
      Map<String, dynamic> body = json.decode(res.body);
      if (body['status'] == 'success') {
        // GetRent getRent = new GetRent();
        // GetOrganised getOrganised = new GetOrganised();
        // GetTrip getTrip = new GetTrip();

        // getOrganised.getorganised();
        // getTrip.gettrips();
        // getRent.getrents();
        launch(body['checkoutUrl']);
      } else if (body['status'] == 'failed') {
        final snackBar = SnackBar(
          duration: new Duration(milliseconds: 7000),
          content: Text(body['checkoutUrl']['data']),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      }
    });
  }
}
