import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_places_dialog/flutter_places_dialog.dart';
import 'package:intl/intl.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/Interfaces/splash.dart';
import 'package:rydeapp/classes/CoachMarks.dart';
import 'package:rydeapp/classes/GoogleMapImp.dart';
import 'package:rydeapp/classes/mappageclass.dart';
//import 'package:rydeapp/classes/mappageclass.dart';
import 'package:rydeapp/main.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart' as cp;

double seatt;

List<String> loca = [];
String dest;
String start;

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage>
    with SingleTickerProviderStateMixin {
  TextEditingController seats = new TextEditingController();
  List<Map<String, dynamic>> loc = [];
  String coachMessage =
      'Tap here to view details of shuttle,\nbook Ryde and preview route on map';
  void tapTile() {
    CoachMarks cm = new CoachMarks(_listKey)
      ..showMark(coachMessage, BoxShape.rectangle);
  }

  String content =
      'We provide a convinient and reliable transport and solution-logistics modules for you';
  var backg = new AssetImage('assets/danfoman.jpg');
  var bus = new AssetImage('assets/bus.png');
  Map<String, dynamic> getroutes;

  double leftpadding = 10.0;

  bool c;

  int lengthget = 0;
  bool _isLoading = true;

  getroute() async {
    var client = new http.Client();
    var response = await client.get('https://ryde-rest.herokuapp.com/alltrips',
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          'x-access-token': token
        });
    getroutes = await json.decode(response.body);
    setState(() {
      try {
        _isLoading = false;
        lengthget = getroutes['trips'].length;
      } catch (e) {
        lengthget = 0;
      }
    });
  }

  String from;
  String to;

  void initState() {
    getroute();
    from = places.elementAt(0);
    to = places.elementAt(0);
    WidgetsBinding.instance.addPostFrameCallback(_afterlayout);
    super.initState();
  }

  _afterlayout(_) {
    Timer t = new Timer(new Duration(milliseconds: 3000), () {
      tapTile();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  static String placeKey = 'AIzaSyDROfLZzWP8-5xX4_XZPKkpxJiVzqzwGqM';
  autoComp() async {}

  List<double> splitLoac = [40.63047005782934, -74.604542375];
  List<double> newSplitLoac = [];

  static DateTime fnow = new DateTime.now();
  int morningT = DateTime(fnow.year, fnow.month, fnow.day, 8, 30)
      .millisecondsSinceEpoch; // set needed date
  int eveningT = DateTime(fnow.year, fnow.month, fnow.day, 16, 30)
      .millisecondsSinceEpoch; // set needed date
  var _timeTemp;
  double mapHeight = 0.0;
  bool showMap = false;
  double lat;
  double long;
  void setForMap() {
    setState(() {
      mapHeight = MediaQuery.of(context).size.height;
    });
  }

  GlobalKey _listKey = GlobalObjectKey('crd');
  GlobalKey<ScaffoldState> firstScaff = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: firstScaff,
        body: new Container(
          child: new RefreshIndicator(
              onRefresh: () async {
                await getroute();
                return Future.delayed(new Duration(milliseconds: 1000));
              },
              child: new Container(
                child: new CustomScrollView(
                  slivers: <Widget>[
                    new SliverAppBar(
                        expandedHeight: 250.0,
                        floating: false,
                        pinned: true,
                        flexibleSpace: new FlexibleSpaceBar(
                          background: new Image(
                            image: backg,
                            fit: BoxFit.cover,
                            colorBlendMode: BlendMode.softLight,
                            color: Colors.black45,
                          ),
                          title: new Text(
                            'Available Rydes!',
                            style: new TextStyle(color: Colors.white),
                          ),
                        )),
                    _isLoading
                        ? new SliverFillRemaining(
                            child: new Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.red,
                            ),
                          ))
                        : new SliverList(
                            delegate: new SliverChildBuilderDelegate(
                                (context, index) {
                              GlobalKey key = index == 0 ? _listKey : null;

                              return new Card(
                                elevation: 0.5,
                                child: new GestureDetector(
                                  child: ListTile(
                                    key: key,
                                    contentPadding: EdgeInsets.all(
                                        MediaQuery.of(context).size.height /
                                            46),
                                    enabled: true,
                                    title: new Text(
                                      getroutes['trips'][index]['ROUTE'] == null
                                          ? 0
                                          : '${getroutes['trips'][index]['ROUTE']}',
                                      style: new TextStyle(
                                          color: Colors.orange, fontSize: 20.0),
                                    ),
                                    subtitle: new Container(
                                      // width: 10.0,
                                      child: StreamBuilder(
                                          stream: Stream.periodic(
                                              Duration(seconds: 1), (i) => i),
                                          builder: (BuildContext context,
                                              AsyncSnapshot<int> snapshot) {
                                            Duration remaining;
                                            DateFormat format =
                                                DateFormat("mm:ss");
                                            int now = DateTime.now()
                                                .millisecondsSinceEpoch;
                                            if (eveningT - now <= 0) {
                                              morningT = DateTime(
                                                      fnow.year,
                                                      fnow.month,
                                                      fnow.day + 1,
                                                      8,
                                                      30)
                                                  .millisecondsSinceEpoch;
                                              _timeTemp = morningT;
                                            } else if (morningT - now < 0) {
                                              _timeTemp = eveningT;
                                            } else {
                                              morningT = DateTime(
                                                      fnow.year,
                                                      fnow.month,
                                                      fnow.day,
                                                      8,
                                                      30)
                                                  .millisecondsSinceEpoch;
                                              _timeTemp = morningT;
                                            }
                                            remaining = new Duration(
                                                milliseconds: _timeTemp - now);

                                            var dateString =
                                                'Departure time: ${remaining.inHours}h:${remaining.inMinutes % 60}m:${remaining.inSeconds % 60}s';
                                            return new Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                new Padding(
                                                  padding: EdgeInsets.all(5.0),
                                                ),
                                                new Container(
                                                  child: new Text(
                                                    '${getroutes['trips'][index]['SEATS']}' ==
                                                            null
                                                        ? 0
                                                        : 'Seats remaining: ${getroutes['trips'][index]['SEATS']}',
                                                    style: new TextStyle(
                                                        fontSize: 18.0),
                                                  ),
                                                ),
                                                new Padding(
                                                  padding: EdgeInsets.all(5.0),
                                                ),
                                                new Container(
                                                  child: new Text(dateString),
                                                ),
                                              ],
                                            );
                                          }),
                                    ),
                                   
                                    onTap: () {
                                      newSplitLoac = splitting(
                                          getroutes['trips'][index]['geoData']
                                              ['STARTLATLONG']);
                                      Navigator.of(context).push(
                                          new MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  GoogleMapImp(
                                                    body: getroutes,
                                                    index: index,
                                                    newSplitLoac: newSplitLoac,
                                                  )));
                                     // showroutedetail(index);
                                      //showbottom(index);
                                    },
                                  ),
                                ),
                              );
                            }, childCount: lengthget == null ? 0 : lengthget),
                          ),
                  ],
                ),
              )),
        ));
  }

  Widget showingGMap() {
    setState(() {
      splitLoac = newSplitLoac.toString() == '[]' ? splitLoac : newSplitLoac;
    });
    print('Oh PleaseEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE $newSplitLoac');
  }

  void showroutedetail(int ind) {
    TimeOfDay x;
    setState(() {
      x = TimeOfDay.now();
    });
    String evening = getroutes['trips'][ind]['EVENING'];
    String morning = getroutes['trips'][ind]['MORNING'];
    getTime(String a) {
      List<String> time = [];
      time = a.split(':');
      return time;
    }

    List<String> m = getTime(morning);
    List<String> e = getTime(evening);
    TimeOfDay morn = TimeOfDay(hour: int.parse(m[0]), minute: int.parse(m[1]));
    TimeOfDay eve = TimeOfDay(hour: int.parse(e[0]), minute: int.parse(e[1]));
    //  d = DateTime.parse(getroutes['trips'][ind]['EVENING']);
    // print('${eve.hour - x.hour}hrs ${x.minute - eve.minute}mins');
    // String eTime = '${eve.hour - x.hour}hrs ${x.minute - eve.minute}mins';
    // String mTime = '${morn.hour - x.hour}hrs ${x.minute - morn.minute}mins';
    // if (eve.hour - x.hour < 0){
    //   eTime = '${(eve.hour - x.hour)+24}hrs ${x.minute - eve.minute}mins';
    // }
    //     if (morn.hour-x.hour<0){
    //   mTime = '${(morn.hour - x.hour)+24}hrs ${x.minute - morn.minute}mins';
    // }

    SimpleDialog sd = new SimpleDialog(
      contentPadding: EdgeInsets.all(0.0),
      children: <Widget>[
        new Container(
          child: new Image(
            image: backg,
            fit: BoxFit.cover,
          ),
          height: MediaQuery.of(context).size.height / 7,
        ),
        new Container(
          margin: EdgeInsets.all(10.0),
          child: new Text(
            'Route: ${getroutes['trips'][ind]['ROUTE']}',
            textAlign: TextAlign.center,
            style: new TextStyle(fontSize: 15.0, color: Colors.orange),
          ),
        ),
        new Container(
          margin: EdgeInsets.all(10.0),
          child: new Text(
            'Morning departure time in: ${getroutes['trips'][ind]['MORNING']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Container(
          margin: EdgeInsets.all(10.0),
          child: new Text(
            'Evening departure time in: ${getroutes['trips'][ind]['EVENING']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Container(
          margin: EdgeInsets.all(10.0),
          child: new Text(
            'Seats Remaining: ${getroutes['trips'][ind]['SEATS']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
          child: new Text(
            'Zone: ${getroutes['trips'][ind]['zone']['NAME']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
          child: new Text(
            'Car description: ${getroutes['trips'][ind]['DESCRIPTION']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
          child: new Text(
            'Car number: ${getroutes['trips'][ind]['CAR_NO']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
          child: new Text(
            'Loc: ${getroutes['trips'][ind]['geoData']['STARTLATLONG']}',
            style: new TextStyle(fontSize: 12.0, color: Colors.black),
          ),
        ),
        new Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new Container(
              child: new OutlineButton(
                onPressed: () async {
                  Navigator.of(context).pop();
                  newSplitLoac = splitting(
                      getroutes['trips'][ind]['geoData']['STARTLATLONG']);
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => GoogleMapImp(
                            body: getroutes,
                            index: ind,
                            newSplitLoac: newSplitLoac,
                          )));
                },
                child: new Text(
                  'Preview in Map',
                  style: new TextStyle(
                    color: Colors.orange,
                    //fontSize: 13.0,
                  ),
                ),
              ),
            ),
            new Container(
              child: new OutlineButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Payscreen(
                              fromTrips: true,
                              description: 'paying for a Trip',
                              tripid: '${getroutes['trips'][ind]['id']}',
                              paymentType: 'trip',
                              item: '${getroutes['trips'][ind]['ROUTE']}',
                              duration: 'monthly',
                              seats: '${getroutes['trips'][ind]['SEATS']}',
                            )),
                  );
                },
                child: new Text(
                  'Book Ryde',
                  style: new TextStyle(
                    color: Colors.orange,
                    // fontSize: 13.0,
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );

    showDialog(context: context, child: sd);
  }

  showbottom(ind) {
    firstScaff.currentState.showBottomSheet((context) {});
  }

  List<double> splitting(String locc) {
    print('Hi im here and Im splitting this like this $locc');
    List<double> l = [];
    List<String> loc = locc.split(",");
    for (int i = 0; i <= loc.length - 1; i++) {
      l.add(double.parse(loc[i]));
    }
    return l;
  }
}
