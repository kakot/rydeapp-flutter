import 'package:flutter/material.dart';
import 'package:rydeapp/classes/http/PostFeedback.dart';
import 'package:rydeapp/customWidgets.dart';

class FeedBackScreen extends StatefulWidget {
  @override
  _FeedBackScreenState createState() => _FeedBackScreenState();
}

TextEditingController _title = new TextEditingController();
TextEditingController _message = new TextEditingController();

class _FeedBackScreenState extends State<FeedBackScreen> {
  bool isloading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.orange[200],
        appBar: appbar('Feedback', context, '/aboutpage'),
        body: new Stack(
          children: <Widget>[
            new Container(
                child: new Center(
                    child: new Container(
              padding: EdgeInsets.all(20.0),
              child: new Card(
                child: new Form(
                    child: new ListView(
                  shrinkWrap: true,
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                        child: new ClipOval(
                      child: new Image.asset(
                        'assets/feedback2.jpg',
                        fit: BoxFit.contain,
                        height: MediaQuery.of(context).size.height / 7,
                      ),
                    )),
                    new Divider(
                      height: 40.0,
                    ),
                    new Container(
                      child: new Text(
                        "Call support line +233 24 358 4253 OR send a mail to info@rydelogistics.com with details of your refund request i.e. Date, Booking/Reference No., Amount.",
                        textAlign: TextAlign.center,
                        style: new TextStyle(color: Colors.black),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10.0),
                      child: new TextFormField(
                        controller: _title,
                        decoration: new InputDecoration(labelText: 'Title:'),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10.0),
                      child: new TextFormField(
                        controller: _message,
                        maxLines: 5,
                        decoration: new InputDecoration(labelText: 'Comment:'),
                      ),
                    ),
                    isloading
                        ? new Center(
                            child: new CircularProgressIndicator(),
                          )
                        : new Container(
                            height: MediaQuery.of(context).size.height / 13,
                            child: new RaisedButton(
                              color: Colors.orange,
                              child: new Text('Send'),
                              onPressed: () async {
                                setState(() {
                                  isloading = true;
                                });
                                var data;
                                PostFeedback feedback = new PostFeedback();
                                data = await feedback.postFeed(
                                    _title.text, _message.text);
                                isloading = false;
                                Navigator.of(context).pop();
                                if (data['status'] == 'success') {
                                  AlertDialog al = new AlertDialog(
                                    content: new Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Container(
                                          child: new Icon(
                                            Icons.check,
                                            color: Colors.orange,
                                          ),
                                        ),
                                        new Text('Thanks for the Feedback!')
                                      ],
                                    ),
                                  );
                                  _title.clear();
                                  _message.clear();
                                  showDialog(context: context, child: al);
                                } else {
                                  AlertDialog al = new AlertDialog(
                                    content: new Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Container(
                                          child: new Icon(
                                            Icons.check,
                                            color: Colors.orange,
                                          ),
                                        ),
                                        new Text(
                                            'Seomthing went wrong please try later')
                                      ],
                                    ),
                                  );
                                  showDialog(context: context, child: al);
                                }
                              },
                            ),
                          )
                  ],
                )),
              ),
            ))),
          ],
        ));
  }
}
