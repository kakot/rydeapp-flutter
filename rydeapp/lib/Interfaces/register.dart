import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

var client = new http.Client();

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => new _RegisterState();
}

var backg = new AssetImage('assets/reseat.jpg');
var logo = new AssetImage('assets/rrydelogo.jpg');
double leftandrighdpadding = 30.0;
double upanddownpadding = 15.0;
Color textcolor = Colors.white;
Color heading = Colors.white;

class _RegisterState extends State<Register> {
  bool isloading = false;
  final formKey = GlobalKey<FormState>();
  TextEditingController name = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController number = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  TextEditingController confirmpass = new TextEditingController();
  bool check = true;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(
          image: backg,
          fit: BoxFit.cover,
        )),
        child: new Container(
          decoration: new BoxDecoration(color: Colors.orange.withOpacity(0.2)),
          child: new ListView(
             // reverse: true,
              children: <Widget>[
                new Container(
                  child: new Image(
                    image: logo,
                    height: 200.0,
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(
                    bottom: 0.0,
                    right: 10.0,
                    left: 10.0,
                  ),
                  child: new Card(
                      elevation: 10.0,
                      child: new Form(
                        key: formKey,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(
                                left: leftandrighdpadding / 2.5,
                              ),
                              margin: EdgeInsets.only(
                                top: leftandrighdpadding,
                                  left: leftandrighdpadding,
                                  right: leftandrighdpadding,
                                  // top: upanddownpadding,
                                  bottom: upanddownpadding),
                              child: new TextFormField(
                                validator: (val) => val.isEmpty
                                    ? 'Please Input Full Name'
                                    : null,
                                controller: name,
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black),
                                //focusNode: new FocusNode(),
                                decoration: new InputDecoration(
                                  labelText: 'Full Name',
                                    contentPadding: EdgeInsets.all(1.5)
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                left: leftandrighdpadding / 2.5,
                              ),
                              margin: EdgeInsets.only(
                                  left: leftandrighdpadding,
                                  right: leftandrighdpadding,
                                  // top: upanddownpadding,
                                  bottom: upanddownpadding),
                              child: new TextFormField(
                                validator: (val) =>
                                    val.isEmpty || !val.contains('@')
                                        ? 'Please Input Valid Mail'
                                        : null,
                                controller: email,
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black),
                                //focusNode: new FocusNode(),
                                decoration: new InputDecoration(
                                  labelText: 'E-mail',
                                    contentPadding: EdgeInsets.all(1.5)
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                left: leftandrighdpadding / 2.5,
                              ),
                              margin: EdgeInsets.only(
                                  left: leftandrighdpadding,
                                  right: leftandrighdpadding,
                                  // top: upanddownpadding,
                                  bottom: upanddownpadding),
                              child: new TextFormField(
                                validator: (val) => val.isEmpty
                                    ? 'Please Input Phone number'
                                    : null,
                                keyboardType: TextInputType.phone,
                                controller: number,
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black),
                                //focusNode: new FocusNode(),
                                decoration: new InputDecoration(
                                  labelText: 'Phone Number',
                                    contentPadding: EdgeInsets.all(1.5)
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                left: leftandrighdpadding / 2.5,
                              ),
                              margin: EdgeInsets.only(
                                  left: leftandrighdpadding,
                                  right: leftandrighdpadding,
                                  // top: upanddownpadding,
                                  bottom: upanddownpadding),
                              child: new TextFormField(
                                obscureText: true,
                                validator: (val) =>
                                    val.isEmpty || val.length <= 7
                                        ? 'Please Input Password Greater than 7'
                                        : null,
                                controller: pass,
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black),
                                //focusNode: new FocusNode(),
                                decoration: new InputDecoration(
                                  labelText: 'Password',
                                    contentPadding: EdgeInsets.all(1.5)
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                left: leftandrighdpadding / 2.5,
                              ),
                              margin: EdgeInsets.only(
                                  left: leftandrighdpadding,
                                  right: leftandrighdpadding,
                                  // top: upanddownpadding,
                                  bottom: upanddownpadding),
                              child: new TextFormField(
                                obscureText: true,
                                validator: (val) =>
                                    !(val.toString() == pass.text)
                                        ? 'Password Mismatch'
                                        : null,
                                controller: confirmpass,
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black),
                                //focusNode: new FocusNode(),
                                decoration: new InputDecoration(
                                  labelText: 'Confirm Password',
                                    contentPadding: EdgeInsets.all(1.5)
                                ),
                              ),
                            ),
                            new Padding(
                              padding: EdgeInsets.only(bottom: 20.0),
                            ),
                            !isloading
                                ? new Container(
                                    width: 700.0,
                                    height: 50.0,
                                    margin: EdgeInsets.only(
                                      top: 20.0,
                                    ),
                                    child: new RaisedButton(
                                      elevation: 5.0,
                                      color: Colors.orangeAccent,
                                      onPressed: () {
                                        postForm(context);
                                      },
                                      child: new Text(
                                        "Signup",
                                        style: new TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ))
                                : new CircularProgressIndicator(),
                          ],
                        ),
                      )),
                )
              ]),
        ),
      ),
    );
  }

  void postForm(BuildContext context) {
    final form = formKey.currentState;
    setState(() {
      isloading = true;
    });
    if (form.validate()) {
      client.post('https://ryde-rest.herokuapp.com/createuser', headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      }, body: {
        "email": email.text.toLowerCase().trim(),
        "password": pass.text,
        'name': name.text.toLowerCase().trim(),
        'phone': number.text
      }).then((response) {
        Map<String, dynamic> body = json.decode(response.body);
        setState(() {
          isloading = false;
        });
        if (body['status'] == 'success') {
          var alert = new AlertDialog(
              title: new Text('Hello ${name.text}'),
              content: new Text(
                'Thanks for Signing up Please head to mail to complete process',
                textAlign: TextAlign.center,
              ));
          showDialog(context: context, child: alert);
          Timer(new Duration(milliseconds: 5000), () {
            Navigator.of(context).pop();
          });        }
      });
    } else {
      setState(() {
        isloading = false;
      });
    }
  }
}
